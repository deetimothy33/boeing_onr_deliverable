\contentsline {chapter}{\numberline {1}ArduPilot Data Generation}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Inserting Attack Code}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Patching AP\_Scheduler.cpp Addresses}{8}{subsection.1.1.1}
\contentsline {section}{\numberline {1.2}Inserting PMU Counter Reading Code}{9}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}HardwareCounterThread}{9}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Modifying AP\_Scheduler.cpp}{10}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Flags in AP\_Scheduler.h}{10}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Main Control Loop Data Generation}{10}{section.1.3}
\contentsline {section}{\numberline {1.4}Task Handler Data Generation}{10}{section.1.4}
\contentsline {section}{\numberline {1.5}Data Generation Scripts}{10}{section.1.5}
\contentsline {section}{\numberline {1.6}Data Generation Commands}{11}{section.1.6}
\contentsline {chapter}{\numberline {2}Machine Learning}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}LibSVM Model Generation}{13}{section.2.1}
\contentsline {chapter}{\numberline {3}Provided Binaries}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}arduplane}{17}{section.3.1}
\contentsline {chapter}{\numberline {4}Video Demo}{18}{chapter.4}
\contentsline {section}{\numberline {4.1}Contents}{19}{section.4.1}
\contentsline {chapter}{\numberline {A}ArduPilot Installation}{20}{appendix.A}
\contentsline {section}{\numberline {A.1}ArduPilot}{20}{section.A.1}
\contentsline {section}{\numberline {A.2}JSBSim}{20}{section.A.2}
\contentsline {section}{\numberline {A.3}ArduPlane WAF Build}{20}{section.A.3}
\contentsline {section}{\numberline {A.4}Bash Shell Environment}{21}{section.A.4}
