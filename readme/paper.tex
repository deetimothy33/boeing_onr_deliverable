\documentclass{report}

\usepackage{verse}
\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{fontspec}
\usepackage{fontawesome}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usepackage{longtable}
\usepackage{verbatim}
\usepackage{listings}

% listings
\lstset{
basicstyle=\small\ttfamily,
columns=flexible,
breaklines=true
}

% title formatting
\usepackage[T1]{fontenc}
\usepackage{titlesec, blindtext, color}
\definecolor{gray75}{gray}{0.75}
\newcommand{\hsp}{\hspace{20pt}}
\titleformat{\chapter}[hang]{\Huge\bfseries}{\thechapter\hsp\textcolor{gray75}{|}\hsp}{0pt}{\Huge\bfseries}

% define block styles
\tikzstyle{decision} = [diamond, draw, fill=blue!20,
    text width=5em, text badly centered, node distance=2.5cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=blue!20,
    text width=25em, text centered, rounded corners, minimum height=4em, node distance=2.5cm]
\tikzstyle{line} = [draw, very thick, color=black!50, -latex']
\tikzstyle{cloud} = [draw, ellipse,fill=red!20, node distance=5cm, minimum height=2em, text width=7em]


\title{Anomaly Detection Using PMU}
\author{Ananda Biswas, Timothy Dee, Zelong Li, Xinyao Li, Akhilesh Tyagi}
\date{}

\begin{document}
\maketitle

\begin{abstract}
	Programs execute different instruction sequences depending on conditional branches.
	These instructions impact the microarchitecture state.
	The Performance Monitoring Unit (PMU) can be configured to count microarchitectural-level events.
	Therefore, anomalous instruction sequences are detectable using PMU counter values.
	Our goal is to verify control flow integrity using these values.

	This project is especially interested in securing embedded systems.
	The PMU is a good candidate for these systems due to its low overhead;
	it executes as a hardware unit separate from the processor.
	A process using the PMU need only configure it and read counter value registers.

	We modify and gather data using ArduPilot as a representative embedded program to test the efficacy of our approach.
	Data is given to LibSVM machine learning libraries to create a model describing PMU counter value changes during execution.
	ArduPilot is again modified to perform online anomaly detection using this model.
	These modifications are done using source code.

	%TODO
	Legacy programs may not have source code available.
	Binary editing using ANGR libraries enables code modification in this case.
	%TODO
\end{abstract}

\tableofcontents
%\part{}


\iffalse
1. Instructions/Readme for ROP/BufferOverflow code in AP_Scheduler
2. Instructions/Readme for hardware_counter_thread/pmu files, what they are, what they do, what counters being used etc.
3. Instructions/Readme on data generation scripts
4. Instructions/Readme on data Interleaving scripts
5. Instructions/Readme on using the generated data in LIBSVM to generate models
6. Figure out ANGR patching, how it works
7. Write an ANGR script for patching PMU+ML into a test binary
8. Write an ANGR script for patching PMU+ML into a AP_Scheduler binary
\fi


% ALPHABATIZE this
\chapter*{Glossary}
\begin{longtable}{p{0.3\hsize}p{0.65\hsize}}
	\faFolder\ ArduPilot & Open-source automated piloting software. 
	Contains modules for several vehicle systems including multi-copters, helicopters, fixed wing aircraft and rovers.\\
	\faFolder\ ArduPlane & A vehicle module part of the ArduPilot program suite.\\
	\faFile\ AP\_Scheduler & Code module within ArduPilot providing task handler scheduling.\\
	\faFolder\ ANGR & Binary editing tool written in python.\\
	Buffer Overflow & Attack performed by overflowing a buffer to modify the return address stored on the program stack. 
	Returning from an afflicted method causes a return to the erroneous address. 
	Other variants write code to the stack an execute it directly.\\
	Control Flow & Instruction sequence during program execution. This depends on conditional branches.\\
	Control Flow Graph (CFG) & A set of possible control flows.\\
	\faFile\ demo.avi & ArduPilot simulation with integrated online machine learning prediction.\\
	PMU Register & 
	Contains PMU counter values or configuration bits.
	PMU registers can be accessed using perf system calls or assembly instructions.\\
	Gadget & Instruction sequence terminated by a return pre-existing in executable code. 
	Frequently occurring at the end of a function.
	Used in ROP attacks.\\
	\faFile\ generate\_one.bash & Start ArduPlane allowing it to execute for sufficient time to generate a log file containing of PMU counter value measurements.\\
	\faFile\ generate\_data.bash & Execute generate\_one.bash multiple times creating a set of log files.\\
	\faFile\ generate\_libsvm\_model.bash & generate a LibSVM model file given an aggregate data file.\\
	\faFile\ HardwareCounterThread & Code developed to provide an API to configure and read PMU registers.
	This is integrated into ArduPlane to generate data.
	This code provides functionality to be run as a separate thread or not.
	We do not use the separate thread functionality when generating data.\\
	\faFile\ interleave\_loop.rb & Combine main control loop data sets generated by generate\_data.bash.\\
	\faFile\ interleave\_task.rb & Combine task handler data sets generated by generate\_data.bash.\\
	\faFolder\ LibSVM & Machine learning library. 
	Provides APIs for model creation and classification.\\
	\faFolder\ Perf System Calls & Performance monitoring software integrated into the Linux kernel.
	The perf API provides for counting performance related events similar to the PMU.
	However, perf uses virtualized counters and requires the overhead of a system call.
	Perf utilizes PMU counter registers for some events.\\
	Performance Monitoring Unit (PMU) & Increments register values on microarchitectural-level events.
	Execution is separate from the processor.
	Thus, register value increments require no overhead.
	A process using the PMU need only configure it and read counter value registers.\\
	\faFile\ pmu\_asm.cpp & Code to configure and read PMU counter values using assembly-level instructions.
	Part of HardwareCounterThread software.\\
	\faFile\ pmu\_perf.cpp & Code to configure and read PMU counter values using perf system calls.
	Part of HardwareCounterThread software.\\
	Return Oriented Programming (ROP) & Attack performed by overflowing a buffer to modify the stack.
	Multiple return addresses are placed on the stack specifying gadget locations.
	These gadgets are executed serially with each gadget return popping the next return address from the stack.\\
	\faFile\ sim\_vehicle.py & ArduPilot simulation script.\\
	Task Handler & Designed to be run at a pre-determined interval. Provides functionality which may include logging, interpreting sensor data, and reacting to sensor data.\\
	\hline
	\faFile & File\\
	\faFolder & Software library\\
\end{longtable}


%\newpage
%\vspace{-10cm}
%\chapter*{Process Diagram}
\begin{figure}[h]
	\centering
	\begin{tikzpicture}[scale=2, node distance = 2cm, auto]
		% Place nodes
		\node [block] (modify) {Modify AP\_Scheduler to include code for PMU counter reading, log file generation, and machine learning prediction.};
		\node [decision, below of=modify] (modchoose) {Using source code?};
		\node [cloud, left of=modchoose] (source) {Source code\\ modification};
		\node [cloud, right of=modchoose] (angr) {Binary editing\\ using ANGR};
		\node [block, below of=modchoose] (env) {Set environment variables for data generation scripts.};
		\node [block, below of=env] (generate) {Generate data sets using generate\_data.bash.};
		\node [block, below of=generate] (interleave) {Combine data sets using interleave\_loop.rb.};
		\node [block, below of=interleave] (train) {Train a LibSVM model using combined data file.};
		\node [block, below of=train] (predict) {Execute ArduPlane to log online anomoly prediction to a file.};
		% Draw edges
		\path [line] (modify) -- (modchoose);
		\path [line] (modchoose) -- node [color=black] {no} (angr);
		\path [line] (modchoose) -- node [color=black] {yes} (source);
		\path [line] (source) -- (env);
		\path [line] (angr) -- (env);
		\path [line] (env) -- (generate);
		\path [line] (generate) -- (interleave);
		\path [line] (interleave) -- (train);
		\path [line] (train) -- (predict);
	\end{tikzpicture}
	\caption{Steps to generate data and test online machine learning prediction.}
	\label{fig-data-generation}
\end{figure}


\chapter{ArduPilot Data Generation}
\section{Inserting Attack Code}
% 1. Instructions/Readme for ROP/BufferOverflow code in AP_Scheduler
%\verbatiminput{attack.txt}
\lstinputlisting{attack.txt}

\subsection{Patching AP\_Scheduler.cpp Addresses}
Buffer overflow and ROP attacks added to AP\_Scheduler.cpp require hard-coded addresses.
A patching script has been written to automatically update the addresses.
This patch script is modified\_ardupilot\_src/patch.bash.

%\lstinputlisting{../modified_ardupilot_src/patch/README}


\section{Inserting PMU Counter Reading Code}
% 2. Instructions/Readme for hardware_counter_thread/pmu files, what they are, what they do, what counters being used etc.
\begin{table}[h]
	\begin{tabular}{p{0.3\hsize}p{0.65\hsize}}
		\hline
		{\bf Counter} & {\bf Description}\\
		\hline
		\multicolumn{2}{c}{\bf Perf System Calls}\\
		INSTRUCTIONS & Retired  instructions. Be careful, these can be affected by various issues, most	notably hardware interrupt counts.\\
		CACHE\_REFERENCES & Cache accesses. Usually this indicates Last Level Cache accesses but this may vary depending on your CPU. This may include prefetches and coherency messages; again this depends on the design of your CPU.\\
		CPU\_CYCLES & Total cycles. Varies based on CPU frequency scaling.\\
		BRANCH\_INSTRUCTIONS & Retired branch instructions. Prior to Linux 2.6.35, this used the wrong event on AMD processors.\\
		BRANCH\_MISSES & Mispredicted branch instructions.\\
		BUS\_CYCLES & Bus cycles, which can be different from total cycles.\\
		\multicolumn{2}{c}{PREF\_COUNT\_HW\_ precedes each of the above counter names.}\\
		\hline
		\multicolumn{2}{c}{\bf Assembly Instructions}\\
		0x08 & Instructions retired.\\
		0x13 & L1 data cache access.\\
		0x11 & CPU cycles.\\
		0xc9 & Conditional branches executed.\\
		0xcc & Conditional branches mispredicted.\\
		0x1d & Bus cycles.\\
		\hline
	\end{tabular}
	\caption{The PMU is configured to use the above counters.}
	\label{tab-counter}
\end{table}

Table~\ref{tab-counter} provides the counters the PMU is configured to use in our experiments.
These have been chosen on the basis of prior testing showing them to be the most potent anomaly indicators.
In addition, using hardware-based perf events enables us to use equivalent events when evaluating 
the use of perf system calls and assembly instructions to read PMU counter registers.

\subsection{HardwareCounterThread}
% what does the HardwareCounterThread class provide?
HardwareCounterThread provides an API for configuring, reading PMU registers and logging counter values to a file.
Logging counter values to a file occurs after a specified number of counter readings;
this avoids incurring log file writing overhead with each counter register read.
There are two mechanisms provided to interact with the PMU.
The ardupilot\_data\_collection/code/src/ directory contains files implementing these functionalities.
pmu\_perf.cpp uses perf system calls.
pmu\_asm.cpp uses assembly instructions.

% How is HardwareCounterThread reading PMU counters
%	perf system calls
%	direct assembly instruction reads
Perf system calls are part of the Linux kernel.
Their API requires system calls which imposes a large overhead.
However, perf provides abstract performance events which PMU counters alone do not.
Perf also virtualizes the counter registers and uses models to subtract overheads such as context switch from the hardware counter values.

Using assembly instructions to read the PMU counter values requires a Linux kernel built with performance monitoring enabled.
Further, user-level access to performance monitoring registers must be enabled from inside the kernel.
This requires the use of a kernel module to set a configuration bit.
Using assembly instructions to read the PMU counter registers minimizes overhead.
However, the absence of a virtualized counters allows other processes to affect the counter values.
%TODO may need to update this sentence
We are currently evaluating the effect of this on classification accuracy.

As the name implies, HardwareCounterThread has APIs for being used as a separate thread.
However, it is not being used as such in our applications.
Its functions operate on the same thread as the process from which they are invoked.

\subsection{Modifying AP\_Scheduler.cpp}
% how is HardwareCounterThread incorporated into AP_Scheudler.cpp
HardwareCounterThread is incorporated into the AP\_Scheduler main loop.
When the forward speed of the aircraft increases beyond 1,
HardwareCounterThread starts the PMU, configures it, and begins reading the pmu counter registers following each main control loop iteration.

\subsection{Flags in AP\_Scheduler.h}
\begin{table}[h]
	\centering
	\begin{tabular}{p{0.3\hsize}p{0.65\hsize}}
		\bf Flag & \bf Description\\
		\hline
		COUNT\_TASK & Counters are read for main event loop when false.
			Counters are read for task handlers when true.\\
		EXPLOIT & Exploits are performed with 50\% frequency when true. 
			Exploits are not performed when false.\\
		\hline
	\end{tabular}
	\caption{Flags contained in AP\_Scheduler.h enable toggling between main control loop data generation and task handler data generation.}
	\label{tab-flag}
\end{table}

Flags are provided as preprocessor directives in AP\_Scheduler.h.
These flags enable switching between {\bf main control loop data generation} and {\bf task handler data generation}.
Table~\ref{tab-flag} provides the available flags and their meaning.


\section{Main Control Loop Data Generation}
Main control loop data generation reads counter values at the end of each control loop iteration.
The interleave\_loop.rb script takes the difference between these counter value readings when generating the aggregate\_data.csv file.
This captures the PMU counter value difference resultant from one main control loop iteration.
Different task handlers are executed in different main control loop iterations.
Therefore, the PMU counters are stopped before task handlers are executed and restarted after they have completed execution.
This limits the PMU counter value increments to the main control loop code only.


\section{Task Handler Data Generation}
Task handler data generation reads counter values before and after task handler execution.
The interleave\_task.rb script takes the difference between these counter value readings when generating the aggregate\_data.csv file.
This enables us to capture the counter value difference from execution of one task handler.
COUNT\_TASK must be set to true in AP\_Scheduler.h for this type of data generation.


\section{Data Generation Scripts}
% 3. Instructions/Readme on data generation scripts
% 4. Instructions/Readme on data Interleaving scripts
\begin{table}[h]
	\centering
	\begin{tabular}{p{0.3\hsize}p{0.3\hsize}p{0.3\hsize}}
		\bf Script & \bf Input / Execution Condition & \bf Output / Effect\\
		\hline
		generate\_one.bash & Used by generate\_data.bash. & One ArduPilot log file. 
			Contains PMU counter readings from 4001 iterations of the main control loop.\\
		generate\_data.bash & Executed to generate a data. & Executes generate\_data.bash multiple times to generate a data set.\\
		interleave\_loop.rb & AP\_HOME/interleave\_data/in/site[0,1,2,3] contain data sets. & 
			AP\_HOME/interleave\_data/out/aggregate\_data.csv is a combined data file. 
			The difference is taken between consecutive lines in the original files.\\
		interleave\_task.rb & \multicolumn{2}{c}{Same as interleave\_loop.rb for task handler data.}\\
		sim\_vehicle.py & Execute within vehicle module directory -- AP\_HOME/ArduPlane/ & Compile and execute the AP\_HOME/build/sitl/arduplane binary.\\
		generate\_libsvm\_model.bash & aggregate\_data.csv & LibSVM model file -- train.data.model\\
		\hline
	\end{tabular}
	\caption{Data generation script input and output descriptions.}
	\label{tab-script-input-output}
\end{table}

%TODO proofread
%TODO add waypoints to script
%	check sim_vehicle.py to see what the default waypoint set is
%TODO add data to repository
%	task handler
%	main control loop
%TODO add binary to repository
%	task handler
%	main control loop

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.3\hsize}p{0.65\hsize}}
		\bf Environment Variable & \bf Description\\
		\hline
		AP\_HOME & Path to ArduPilot directory.\\
		BOD\_HOME & Path to the boing\_onr\_deliverable directory.\\
		\multicolumn{2}{c}{{\tt export VAR=value} can be used to set environment variables.}\\
		\hline
	\end{tabular}
	\caption{Data generation scripts use environment variables.
	This table provides environment variables which must be set and their meaning.}
	\label{tab-path-change}
\end{table}

\iffalse
\begin{figure}
	\centering
	\begin{tikzpicture}[scale=2, node distance = 2cm, auto]
		% Place nodes
		\node [block] (modify) {Modify AP\_Scheduler to include code for PMU counter reading, log file generation, and machine learning prediction.};
		\node [block, below of=modify] (generate) {Generate data sets using generate\_data.bash.};
		\node [block, below of=generate] (interleave) {Combine data sets using interleave\_loop.rb.};
		\node [block, below of=interleave] (train) {Train a LibSVM model using combined data file.};
		\node [block, below of=train] (predict) {Execute ArduPlane to log online anomaly prediction to a file.};
		% Draw edges
		\path [line] (modify) -- (generate);
		\path [line] (generate) -- (interleave);
		\path [line] (interleave) -- (train);
		\path [line] (train) -- (predict);
	\end{tikzpicture}
	\caption{Steps to generate data.}
	\label{fig-data-generation}
\end{figure}
\fi

Data generation is a multi-step process shown in Figure~\ref{fig-data-generation}.
After modifying AP\_Scheduler to include attacks, machine learning, and PMU code,
generate\_data.bash is executed to create a data set.
sim\_vehicle.py must be executed once in the ArduPlane directory to pre-compile the code.
Otherwise, the timing of generate\_data.bash will be thrown off.
generate\_data.bash executes generate\_one.bash multiple times.
Each execution of generate\_one.bash produces one log file.
This log file contains PMU counter readings.
Each log file line corresponds to a reading at the end of the AP\_Scheduler loop function.
interleave\_data.bash combines all the data sets generated by generate\_data.bash.
It takes the difference between consecutive lines.
The result is a file with each line containing counter value differences resultant from one control loop iteration.
These counter value differences are used as features to train the machine learning classifier.

Data generation scripts can be found in the ardupilot\_data\_collection/ directory.
Their inputs and outputs are described below in Table~\ref{tab-script-input-output}.
%These scripts contain absolute path names requiring correction for their target system.
%Table~\ref{tab-path-change} gives the path names requiring change.
These scripts use environment variables to locate the ArduPilot installation and deliverable directory.
Table~\ref{tab-path-change} describes relevant variables which must be set for the scripts to function.
{\tt export VAR=value} is one command to accomplish this.

\section{Data Generation Commands}
\verbatiminput{../ardupilot_data_collection/readme.txt}


\chapter{Machine Learning}
LibSVM model generation tools are found in the mloffline/code directory.
The input is a data file generated by the interleave.rb script.
The output is a model which will be used for online machine learning prediction.
The model is read in the AP\_Scheduler init function.
The predict function at the end of the AP\_Scheduler main loop utilizes this model to predict whether the PMU counter value changes resultant from the preceding iteration indicate a control flow anomaly.

The generate\_libsvm\_model.bash script generates a libsvm model for online machine learning prediction.
It moves this model file to the directory in ArduPilot where the AP\_Scheduler.cpp code expects it to be.
This must be executed prior to online machine learning prediction.

\section{LibSVM Model Generation}
% 5. Instructions/Readme on using the generated data in LIBSVM to generate models
\verbatiminput{../mloffline/README.TXT}


\iffalse
\chapter{ANGR Patches}
% 6. Figure out ANGR patching, how it works


\section{Patching Test Binary}
% 7. Write an ANGR script for patching PMU+ML into a test binary


\section{Patching AP\_Scheudler.o}
% 8. Write an ANGR script for patching PMU+ML into a AP_Scheduler binary
\fi


\chapter{Provided Binaries}
\iffalse
\begin{verbatim}
	git clone https://github.com/ArduPilot/ardupilot.git
	cd ardupilot
	git reset --hard ceab853c86f92767e8565fb8c2e7dca4e691415f
\end{verbatim}
\fi

\begin{table}[h]
	\centering
	\begin{tabular}{p{0.3\hsize}p{0.65\hsize}}
		\bf Binary & \bf Description\\
		\hline
		arduplane & Main control loop modifications. Includes all modifications including data logging, PMU counter reading, and machine learning prediction.\\
		arduplane\_task & Task Handler modifications. Includes all modifications including data logging, PMU counter reading, and machine learning prediction.\\
		\hline
	\end{tabular}
	\caption{Provided binaries and their use.}
	\label{tab-binary}
\end{table}

Table~\ref{tab-binary} describes the provided binaries.

Sample data generated using the binaries is provided.

\section{arduplane}
The waf build system compiles each ArduPilot source file independently.
The last step of the build process links them all creating a single ArduPilot binary.
We have provided versions of this binary including all of our modifications.
To use this binary execute the following commands.

\begin{verbatim}
# enter the ArduPlane directory
cd $AP_HOME/ArduPlane/
# compile ArduPlane
../Tools/autotest/sim_vehicle.py
# replace the generated binary with our arduplane binary
cp $BOD_HOME/binary/arduplane $AP_HOME/build/sitl/bin/arduplane
# execute the modified binary
../Tools/autotest/sim_vehicle.py
\end{verbatim}


\chapter{Video Demo}
\begin{figure}[h]
	\centering
	\includegraphics[width=\hsize]{control_flow.pdf}
	\caption{AP\_Scheduler main control loop diagram.}
	\label{fig-control-flow}
\end{figure}

A video demo of an ArduPilot simulation with integrated online machine learning prediction is provided as demo.avi.

\section{Contents}
We have been developing a method to use PMU counter values to detect anomalies on embedded systems.
As a representative embedded system we have chosen ArduPilot in order to test our schemes.
Here we have the code structure of ArduPilot.
There are several vehicle modules within ArduPilot.
Each vehicle module calls the AP\_Scheduler loop function repeatedly.
A schedule is established prior to execution.
The AP\_Scheudler loop is the mechanism by which the schedule is executed.

The AP\_Scheduler loop function performs the following steps after modification as shown in Figure~\ref{fig-control-flow}.
First, we choose to start the PMU counters based on when the vehicle begins moving.
This is accomplished by testing the aircraft speed.
The program then waits for an inertial sample and computes the time available to execute task handlers.
Task handlers are executed at the frequency defined by the schedule.
After all task handlers have been completed we read the PMU counter registers.

Reading PMU counter registers yields a vector of integers.
We use machine learning techniques to determine whether this vector of integers indicates an anomaly.
The machine learning library we are using is LibSVM. % as shown in Figure~\ref{}.
In particular, we are using SVM with linear kernel from this software package.
Training involves gathering a data set where the PMU counters from the previous iteration of the main control loop have indicated an attack, namely we simulate a buffer overflow or an ROP attack and measure the counter values resultant from that, or we gather the PMU counter values where there has not been an attack.
This creates our two classes for machine learning, either attack or non-attack.
We train LibSVM offline using these and integrate the predict function into ArduPilot.

We use ArduPilot simulation in order to demonstrate our online machine learning prediction.
We enter the directory of the ArduPlane vehicle submodule.
ArduPilot simulations use the sim\_vehicle.py script to create an execution environment, compile, and execute the code.
There are two attacks that have been implemented in the ArduPlane code.
One of them being an ROP attack where we call three gadget modules and return to the main control loop.
The other being a buffer overflow attack.
We call one function that would not occur in the normal control flow by overflowing a buffer such that the return address on the stack is overwritten and then return to the main control loop.
We modify the ArduPlane init function to read in the training data for our machine learning code.
This training data is then used by the predict function online in order to make predictions based on the PMU counter readings.
This predict function was taken from LibSVM source code and integrated into ArduPlane.
After starting up, "mode auto" and "arm throttle" are required inputs to start the plane flying around waypoints in the simulated environment.
These waypoints are always the same so for our data collection and test executions it creates a consistent environment.

A log file is generated while ArduPilot is executing.
We will use this to generate our online machine learning prediction.
Important outputs include "Attack/Non-Attack n" and "prediction m".
n is 1 if an attack occurs in the current main control loop iteration and -1 otherwise.
m is 1 if the PMU counter values resultant from the current main control loop iteration indicate an attack and -1 otherwise.
The prediction is correct is n==m.


\appendix


\chapter{ArduPilot Installation}
The following commands are used to complete a fresh install of ArduPilot on an x86 machine under Ubuntu 16.04.6.


\section{ArduPilot}
ArduPilot installation documentation is provided at:\\
\url{http://ardupilot.org/dev/docs/building-setup-linux.html#building-setup-linux}

\begin{verbatim}
git clone https://github.com/ArduPilot/ardupilot
cd ardupilot
git submodule update --init --recursive
chmod +x Tools/environment_install/install-prereqs-ubuntu.sh
./Tools/environment_install/install-prereqs-ubuntu.sh -y

. ~/.profile
cd ..   
\end{verbatim}


\section{JSBSim}
JSBSim installation documentation is provided at:\\
\url{http://ardupilot.org/dev/docs/sitl-with-jsbsim.html}

\begin{verbatim}
git clone git://github.com/JSBSim-Team/jsbsim.git
cd jsbsim
mkdir build
cd build
cmake -DCMAKE_CXX_FLAGS_RELEASE="-O3 -march=native -mtune=native" -DCMAKE_C_FLAGS_RELEASE="-O3 -march=native -mtune=native" -DCMAKE_BUILD_TYPE=Release ..
make -j5
\end{verbatim}


\section{ArduPlane WAF Build}
In the ArduPilot directory executing the following commands will configure ArduPilot Software In The Loop (SITL) simulations.
This only needs to be done once.

\begin{verbatim}
(configure SITL here)
./waf configure --board sitl --debug        
(builds all targets, alternatively use flag '--target bin/arduplane' for single target)
./waf                                       
\end{verbatim}

To execute the ArduPlane simulation execute the following commands.

\begin{verbatim}
(goto ArduPlane and run)
cd ArduPlane
sim_vehicle.py

(if sim_vehicle.py doesn't work, do either)
1. ../Tools/autotest/sim_vehicle.py  
OR 
2. cd ..
. ~/.profile
cd ArduPlane
\end{verbatim}


\section{Bash Shell Environment}
In order to execute ArduPilot simulations, we set a number of environment variables.
These updates affect ArduPilot compilation and simulation.
The following commands are place in the ~/.bashrc files on our test systems.

\begin{verbatim}
export PATH=$PATH:$HOME/jsbsim/src
export PATH=$PATH:$HOME/ardupilot/Tools/autotest
export PATH=/usr/lib/ccache:$PATH
export CXXFLAGS='-fno-stack-protector -Wno-narrowing'
export CFLAGS='-fno-stack-protector -Wno-narrowing'
export AP_HOME=/home/pi/ardupilot
export BOE_HOME=/home/pi/boeing_onr_deliverable
\end{verbatim}


\end{document}
