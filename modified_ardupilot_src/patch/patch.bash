#patch_address.bash

#!/bin/bash

echo "INFO: patch.bash works using AP_HOME env variable set to ArduPilot top directory"
echo " "
if [ "$1" == "c" ] 
then
    echo "removing .old files"
    rm *.old*

elif [ "$1" = "p" ]
then
	echo "Fetching log file"
	if [ ! -f "/tmp/ArduPlane.log" ]; then
		echo "ArduPlane.log not found, exiting..."
		echo "Probable cause: when xterm is installed, Ardupilot does not "
		echo "generate a log file in /tmp. Try disabling xterm"
		exit 1
	fi
	cp /tmp/ArduPlane.log .
	echo "Compiling address_patch.cpp"
	make address_patch
	echo "Running address patch"
	./address_patch
    if [ "$2" = "b" ]
    then
 
	    echo "Backing up source files(as .old*)"
	    name0=AP_Scheduler.cpp.old
	    name1=ec.h.old

	    if [[ -e $name0 ]] ; then
	    	i=0
	    	while [[ -e $name0$i ]] ; do
				let i++
	    	done
	    	name0=$name0$i
	    fi
	    	mv $AP_HOME/libraries/AP_Scheduler/AP_Scheduler.cpp $name0

	    if [[ -e $name1 ]] ; then
	    	i=0
	    	while [[ -e $name1$i ]] ; do
			let i++
	        done
	        name1=$name1$i
	    fi
	    mv $AP_HOME/libraries/AP_Scheduler/ec.h $name1
	fi
	echo "Replacing old files with patched ones"
	mv AP_Scheduler2.cpp $AP_HOME/libraries/AP_Scheduler/AP_Scheduler.cpp
	mv ec2.h $AP_HOME/libraries/AP_Scheduler/ec.h

	echo "Done!"
else
	echo "Usage: ./patch_address <option>"
	echo "options: c     :: remove all files with .old* extension"
	echo "         p     :: patch addresses"
	echo "         p b   :: backup files before patching"

fi
