

/* ---------------------------------------------------------------------------------------------------------- */
	// top 4 lines added
	asm volatile ("MOV %0, pc\n" : "=r" (return_address)); //for debugging, may be removed
	asm volatile ("MOV %0, sp\n" : "=r" (s_p)); //for debugging, may be removed
	printf("\n\nAddress before caller %x\n", return_address); //for debugging, may be removed
	printf("SP before caller %x\n", s_p); //for debugging, may be removed
	
	printf("Before calling exploit_caller(): exploit_b =  %d\n", exploit_b);
	//save R7,R8, and use as temp registers
	asm volatile("MOV %0, R7\n" :"=r" (ra)); //save R7 into var ra
	asm volatile("MOV %0, R8\n" :"=r" (rb)); //save R8 into var rb

	int x = exploit_caller( /*exploit_b && (hwc.running)*/);
	
	//Restore registers. 
	//Addresses below may need change if code is added/removed from AP_Scheduler
/* Register Addresses */
	asm volatile ("LDR r7,=#0x15ac8c\n"); //load address of RA into r7. 
	asm volatile ("LDR r7, [r7]\n"); //restore value from address of RA into R7		
	asm volatile ("LDR r8,=#0x15ac90\n"); //load address of RB into r8
	asm volatile ("LDR r8, [r8]\n"); //restore value from address of RB into R8
	
	std::cout << "BACK FROM THE EXPLOIT!!  rand: "<< exploit_b << std::endl;
/* ---------------------------------------------------------------------------------------------------------- */
