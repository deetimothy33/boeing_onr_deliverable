def output_aggregate_data(output_file="out/aggregate_data.csv", random=false)
	puts "aggregating output #{output_file}"
		
	# randomly pick a key
	# give more weight to "no-attak" key (-1)
	#
	# generate a random number betwene 0 and 199
	# if the number is a key in the hash,
	# then take a line from this key
	# if the number is not a key in the hash,
	# then include a line from not-attack
	# 
	# delete entries after use.
	# stop when there are no entries to use.
	
	output=File.open(output_file,'w')
	
	if random
		# randomly interleave the file data
		while true
			# this must be changed based on the ratio of no-attack to attack
			# its value is fraction of:
			# 	(number of a single attack site lines) / (total number of lines)
			r=rand(8)
			if $site_map.has_key?("#{r}")
				# attack
				if $site_map["#{r}"].length()>0
					output << $site_map["#{r}"].pop
				else
					break
				end
			elsif $site_map.has_key?("#{-1}") 
				# no attack
				if $site_map["#{-1}"].length()>0
					output << $site_map["-1"].pop
				else
					break
				end
			end
		end
	else
		# output each key until you run out of data
		# then output all data for the next key
		# ... and so on. Until there are no keys
		$site_map.each do |key, value| 
			#puts "#{key}"
			while value.length>0
				output<<value.pop
			end
		end
	end

	# print how much data is USED
	$site_map.each { |key, value| puts "#{key}:\t#{value.length}" }

	output.close
end

