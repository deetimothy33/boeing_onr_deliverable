#! /usr/bin/ruby

# this script takes interleaves TASK data files
# data files are generated using ArduPilot
# each set of data files contains distinct attack cites or no attack cite
#
# difference between data file lines are taken
# this makes each measurement independent
# they all represent the counter increment over one loop
#
# in/
# 	task_xx/
#
# out/
# 	aggregate_data.csv

require 'csv'
require_relative 'output_aggregate'

INPUT="in"
OUTPUT="out/aggregate_data.csv"

# keys are attack sites
# values are lists of file lines
$site_map={}


def read_file(file_name)
	puts "parsing #{file_name}"

	# read the input file
	# parse each line into a category based on the attack cite
	prev_split_line=nil
	File.readlines(file_name).each do |line|
		# extract attack_site from the line
		split_line=line.split(',')
		# only do stuff if there is a previous line to subtract from
		if not prev_split_line.nil?
			attack_site=split_line[0]

			# if information for the attack site doesn't exist yet,
			# then create it
			if not $site_map.has_key?(attack_site)
				$site_map[attack_site]=Queue.new
			end

			# subtract this line from the previous line
			# this makes the data readings independent from one-another
			diff_line=[]
			diff_line<<split_line[0].to_i
			(1..6).each do |i|
				diff_line << split_line[i].to_i - prev_split_line[i].to_i
			end
			diff_line<<split_line[7].to_i

			# create a string in csv format
			csv_string=CSV.generate do |csv|
				csv << diff_line
			end

			# add line to the entry for the attack site
			$site_map[attack_site] << csv_string
			
			# set prev_split_line to nil
			# this causes the next line difference to be skipped
			prev_split_line=nil
		else
			prev_split_line=split_line
		end
	end
end

def main()
	#thread_a=[]
	Dir.foreach("#{INPUT}") do |item|
		next if item=='.' or item=='..'
		# for each folder,
		# iterate over all files
		Dir.foreach("#{INPUT}/#{item}") do |file|
			next if file=='.' or file=='..'
			# this is very slow without multi-threading
			#thread_a<<Thread.new{read_file("#{INPUT}/#{item}/#{file}")}
			read_file("#{INPUT}/#{item}/#{file}")
		end
	end

	# wait for all threads to complete before aggregating data

	output_aggregate_data()
end

main()
