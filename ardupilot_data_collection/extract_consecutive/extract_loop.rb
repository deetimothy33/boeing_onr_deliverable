#! /usr/bin/ruby

# this script takes interleaves data files
# data files are generated using ArduPilot
# each set of data files contains distinct attack cites or no attack cite
#
# difference between data file lines are taken
# this makes each measurement independent
# they all represent the counter increment over one loop
#
# Lines are grouped into by attack cite
# this creates 5 groupings ( for main control loop )
# These grouping are then randomly woven together to create an aggregated data file
#
# in/
# 	site_1/
# 	site_2/
# 	site_3/
# 	site_4/
# 	no_attack/
#
# out/
# 	aggregate_data.csv

require 'csv'

INPUT="in"
OUTPUT="out"

# keys are attack sites
# values are lists of file lines
$site_map={}


def read_file(file_name)
	puts "parsing #{file_name}"

	# only put things into the hash under the desired condition
	# the condition is two consecutive lines with the same attack_site

	# read the input file
	# parse each line into a category based on the attack cite
	prev_split_line=nil
	prev_diff_line=nil
	File.readlines(file_name).each do |line|
		# extract attack_site from the line
		split_line=line.split(',')
		# only do stuff if there is a previous line to subtract from
		if not prev_split_line.nil?
			attack_site=split_line[0]

			# if information for the attack site doesn't exist yet,
			# then create it
			if not $site_map.has_key?(attack_site)
				$site_map[attack_site]=Queue.new
			end

			# subtract this line from the previous line
			# this makes the data readings independent from one-another
			diff_line=[]
			diff_line<<split_line[0].to_i
			(1..6).each do |i|
				diff_line << split_line[i].to_i - prev_split_line[i].to_i
			end
			diff_line<<split_line[7].to_i

			# create a string in csv format
			csv_string=CSV.generate do |csv|
				csv << diff_line
			end

			# add line to the entry for the attack site
			if (not prev_diff_line==nil) and (diff_line[0]==prev_diff_line[0])
				prev_diff_line[0]=prev_diff_line[0]+1000
				prev_csv_string=CSV.generate do |csv|
					csv << prev_diff_line
				end
				$site_map[attack_site] << prev_csv_string
				$site_map[attack_site] << csv_string
				# clear prev_diff_line to prevent the same record being added twice
				# makes it as if it is the first time through the loop
				# this skips one line
				#TODO this could be improved
				prev_split_line=nil
				next
			end
		end
			
		prev_split_line=split_line
		prev_diff_line=diff_line
	end
end

def print_hash()
	puts "aggregating output #{OUTPUT}"
		
	# generate one file for each site
	# 	corresponds to each positive key in the hashmap
	# generate one file for non-attack data
	# 	corresponds to -1 in the hashmap
	#
	# For each key in the hash
	$site_map.each do |key, value|
		File.open("#{OUTPUT}/site#{key}.csv",'w') do |file|
			until value.empty? do
				file << value.pop
			end
		end
	end
end

def main()
	Dir.foreach("#{INPUT}") do |item|
		next if item=='.' or item=='..'
		# for each folder,
		# iterate over all files
		Dir.foreach("#{INPUT}/#{item}") do |file|
			next if file=='.' or file=='..'
			read_file("#{INPUT}/#{item}/#{file}")
		end
	end

	print_hash()
end

main()
