#include "main.h"

// this code tests functionality
// of other code

int main(){
	//pmu_example();
	//return 0;

	/*
	// hardware counter thread
	HardwareCounterThread hct;
	hct.frequency=1000;
	hct.number=10000;
	hct.log_file_name="log.csv";
	hct.start();
	while(hct.running==true);	
	hct.stop();
	
	// hardware counter direct pmu
	HardwareCounterThread hct0;
	hct0.log_file_name="direct.csv";
	hct0.counter_enable();
	hct0.counter_read();
	hct0.counter_stop();
	hct0.counter_read();
	hct0.counter_read();
	hct0.counter_start();
	hct0.counter_read();
	hct0.counter_disable();
	hct0.counter_log();
	*/

	// branch recording
	HardwareCounterThread hct1;
	hct1.log_file_name="branch.csv";
	hct1.counter_enable();
	
	hct1.branch_record(1,true);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	hct1.branch_record(1,false);
	
	//hct1.branch_record(0,true);
	hct1.counter_read(1);
	hct1.counter_disable();
	hct1.counter_log();

	// buffer overflow 
	//TODO

	// test
	//std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	return 0;
}
