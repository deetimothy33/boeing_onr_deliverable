#ifndef HARDWARE_COUNTER_THREAD_H
#define HARDWARE_COUNTER_THREAD_H

#include <thread>
#include <chrono>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <time.h>
#include "pmu.h"

//**added for ML
#include "svm.h"
#include "ml.h"
//extern static char *line = NULL;
//static int max_line_len;
extern struct svm_node *nodex;
extern int max_nr_attr;
extern char* line;
//**

// spawn a thread to
// 1 read hardware counters at some frequency
// 2 log values to file after some number of reads

class HardwareCounterThread{
	public:
		// number of times counter_read() has been called
		int count;
		// hardware counter read frequency (ns)
		long frequency=1000;
		// number of measurements
		int number=10000;
		std::string log_file_name="perf_log/loop.csv";
		std::vector<pmu::counter_t> counter_v;
		std::vector<int> attack_bool_v;
		std::vector<unsigned> branch_history_v;
		unsigned branch_history=0;
		std::map<int,unsigned> branch_history_map;
		bool running=false;

		HardwareCounterThread();

		// THREAD FUNCTIONS

		// start thread
		void start();
		// stop thread
		void stop();

		// PMU INTERFACE FUNCTIONS

		// enable pmu counters AND start pmu counters
		// AND open file discriptors
		void counter_enable();
		// diable pmu counters
		// close file discriptors
		void counter_disable();
		// set up pmu counters
		void counter_setup();
		// start counters, do NOT start thread
		void counter_start();
		// stop counters, do NOT start thread
		void counter_stop();
		// read current counter values
		// store in memory
		void counter_read(int attack=-1);
		// log current counters
		// clear stored memory
		void counter_log();

		// BRANCH HISTORY FUNCTIONS

		// record a branch history
		// indentifier -- identifies the branch
		// taken -- true if the branch is taken
		void branch_record(int identifier, bool taken);
	private:
		bool stop_thread=false;
		std::thread t;

		void read_loop();
		void log_counters();
};

#endif
