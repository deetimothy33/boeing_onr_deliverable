#ifndef BUFFER_OVERFLOW_H
#define BUFFER_OVERFLOW_H


// REFERENCES
// https://stackoverflow.com/questions/28287256/buffer-overflow-implementation#
// https://en.wikipedia.org/wiki/ARM_architecture
// http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/ka3540.html
// https://stackoverflow.com/questions/1693011/how-can-i-determine-the-return-address-on-stack#1693025
// 	void * __builtin_return_address (unsigned int level)
// http://www.keil.com/support/man/docs/armcc/armcc_chr1359124249383.htm


namespace buffer_overflow{
	unsigned int get_stack_pointer(void);
	unsigned int get_return_address(void);
	unsigned int get_return_address_address(void);
}

#endif

