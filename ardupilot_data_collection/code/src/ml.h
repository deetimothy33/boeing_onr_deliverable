#ifndef ML_H
#define ML_H

extern FILE *fp_restore,*out, *testfile;
extern struct svm_model* modl;
extern char *ll, *line;

#ifndef SKIP_TARGET 
#define SKIP_TARGET\
	while(isspace(*p)) ++p;\
	while(!isspace(*p)) ++p;
#endif

#ifndef SKIP_ELEMENT 
#define SKIP_ELEMENT\
	while(*p!=':') ++p;\
	++p;\
	while(isspace(*p)) ++p;\
	while(*p && !isspace(*p)) ++p;
#endif

#endif
