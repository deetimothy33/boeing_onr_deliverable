#include "hardware_counter_thread.h"
#include <math.h>

#ifndef STD_LIBRARIES
#define STD_LIBRARIES
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <cstdlib>
#endif

#include <float.h>
#include <stdarg.h>
#include <limits.h>
#include <locale.h>
//#include "svm.h"
HardwareCounterThread::HardwareCounterThread(){
}

//ML STUFF
int print_null(const char *s,...) {return 0;}

static int (*info)(const char *fmt,...) = &printf;

int predict_probability=0;
int max_line_len = 1024;

static char* readline(FILE *input)
{
	int len;

	if(fgets(line,max_line_len,input) == NULL)
		return NULL;

	while(strrchr(line,'\n') == NULL)
	{
		max_line_len *= 2;
		line = (char *) realloc(line,max_line_len);
		len = (int) strlen(line);
		if(fgets(line+len,max_line_len-len,input) == NULL)
			break;
	}
	return line;
}

void exit_input_error(int line_num)
{
	fprintf(stderr,"Wrong input format at line %d\n", line_num);
	exit(1);
}
void exit_with_help()
{
	printf(
	"Usage: svm-predict [options] test_file model_file output_file\n"
	"options:\n"
	"-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); for one-class SVM only 0 is supported\n"
	"-q : quiet mode (no outputs)\n"
	);
	exit(1);
}

clock_t time_taken;
double cpu_time = 0;

void predict(char *input)
{	//std::cout<<"Inside Predict"<<"\n";
	int correct = 0;
	int total = 0;
	double error = 0;
	double sump = 0, sumt = 0, sumpp = 0, sumtt = 0, sumpt = 0;

	int svm_type=svm_get_svm_type(modl);
	int nr_class=svm_get_nr_class(modl);
	double *prob_estimates=NULL;
	//int j;


	//printf("In predict %s\n",input);
	max_line_len = 1024;
	//line = (char *)realloc(line,max_line_len*sizeof(char));
	//memcpy(line,input,sizeof(char)*strlen(*input));
	//while(readline(attack, input) != NULL)
	//{
		int i = 0;
		double target_label, predict_label;
		char *idx, *val, *label, *endptr;
		int inst_max_index = -1; // strtol gives 0 if wrong format, and precomputed kernel has <index> start from 0

		label = strtok(input," \t\n");
		if(label == NULL) // empty line
			exit_input_error(total+1);

		target_label = strtod(label,&endptr);
		if(endptr == label || *endptr != '\0')
			exit_input_error(total+1);

		while(1)
		{
			if(i>=max_nr_attr-1)	// need one more for index = -1
			{
				max_nr_attr *= 2;
				nodex = (struct svm_node *) realloc(nodex,max_nr_attr*sizeof(struct svm_node));
			}

			idx = strtok(NULL,":");
			val = strtok(NULL," \t");

			if(val == NULL)
				break;
			errno = 0;
			nodex[i].index = (int) strtol(idx,&endptr,10);
			if(endptr == idx || errno != 0 || *endptr != '\0' || nodex[i].index <= inst_max_index)
				exit_input_error(total+1);
			else
				inst_max_index = nodex[i].index;

			errno = 0;
			nodex[i].value = strtod(val,&endptr);
			if(endptr == val || errno != 0 || (*endptr != '\0' && !isspace(*endptr)))
				exit_input_error(total+1);

			++i;
		}
		nodex[i].index = -1;

		{
			predict_label = svm_predict(modl,nodex);

			printf("Attack/Non-Attack:Prediction %s %.17g\n",label,predict_label);
			//printf("prediction %.17g\n",predict_label);
		}

		if(predict_label == target_label)
			++correct;
		error += (predict_label-target_label)*(predict_label-target_label);
		sump += predict_label;
		sumt += target_label;
		sumpp += predict_label*predict_label;
		sumtt += target_label*target_label;
		sumpt += predict_label*target_label;
		++total;
	//}
	
}
//ML STUFF END
//SCALE STUFF
double y_lower,y_upper;
int y_scaling = 0;

double feature_max[8];
double feature_min[8];
double lower;
double upper;
double pprime[8];
double mprime[8];

double y_max = -DBL_MAX;
double y_min = DBL_MAX;
int max_index;
int min_index;
long int num_nonzeros = 0;
long int new_num_nonzeros = 0;

#define max(x,y) (((x)>(y))?(x):(y))
#define min(x,y) (((x)<(y))?(x):(y))


//SCALE STUFF END
void HardwareCounterThread::start(){
	counter_v.clear();
	running=true;
	t=std::thread(&HardwareCounterThread::read_loop,this);
}

void HardwareCounterThread::stop(){
	stop_thread=true;
	t.join();
}

void HardwareCounterThread::counter_enable(){
	pmu::counter_setup();
	running=true;
	count=0;
}

void HardwareCounterThread::counter_disable(){
	pmu::counter_disable();
	running=false;
}

void HardwareCounterThread::counter_start(){
	if(running) pmu::counter_start();
}

void HardwareCounterThread::counter_stop(){
	if(running) pmu::counter_stop();
}

void HardwareCounterThread::counter_read(int attack){
	//printf("Inside counter_read() attack=%d\n",attack);
	
	pmu::counter_t c;
	pmu::counter_t cp;
	pmu::counter_read(&c);
	if(running){
		(*this).counter_v.push_back(c);
		(*this).attack_bool_v.push_back(attack);
		(*this).branch_history_v.push_back(branch_history_map[attack]);
		//std::cout << branch_history_map[attack] << std::endl;
		
		count++;
		
		// PREDICT
		if(counter_v.size()>1){
			// get the last element
			cp=counter_v[counter_v.size()-2];
		}else{
			cp.c0=0;
			cp.c1=0;
			cp.c2=0;
			cp.c3=0;
			cp.c4=0;
			cp.c5=0;
		}


		//readline(testfile);
		//printf("Counters %s\n",line);
		//scale(&attack, &c);
		line = (char *)realloc(line,2*max_line_len*sizeof(char));
		sprintf(line,"%d 1:%lu 2:%lu 3:%lu 4:%lu 5:%lu 6:%lu",attack,
				c.c0-cp.c0,c.c1-cp.c1,c.c2-cp.c2,c.c3-cp.c3,c.c4-cp.c4,c.c5-cp.c5);
	//	printf("Act Counters 1:%lu 2:%lu 3:%lu 4:%lu 5:%lu 6:%lu\n",
	//			c.c0-cp.c0,c.c1-cp.c1,c.c2-cp.c2,c.c3-cp.c3,c.c4-cp.c4,c.c5-cp.c5);
	//	printf("Counters %s\n",line);
		time_taken = clock();

		predict(line);
		
		time_taken = clock() - time_taken;
		cpu_time = ((double)time_taken)/CLOCKS_PER_SEC;
		printf("Time to Read = %f\n",cpu_time);

		//if(feof(testfile))
		//	exit(1);	
	}
}

void HardwareCounterThread::counter_log(){
	log_counters();
	counter_v.clear();
	attack_bool_v.clear();
	branch_history_v.clear();
}

void HardwareCounterThread::read_loop(){
	count=0;
	auto time=std::chrono::high_resolution_clock::now();

	pmu::counter_setup();
	while(count<number && !(*this).stop_thread){
		// read counters
		// store values in memory
		//pmu::counter_t c;
		// measure every 'frequency' nanoseconds
		std::this_thread::sleep_for(std::chrono::nanoseconds((*this).frequency) - 
				(std::chrono::high_resolution_clock::now()-time));
		time=std::chrono::high_resolution_clock::now();
		HardwareCounterThread::counter_read(-1);
		//pmu::counter_read(&c);
		//(*this).counter_v.push_back(c);
		//(*this).attack_bool_v.push_back(false);

		count++;
	}
	pmu::counter_disable();

	// log values to file
	log_counters();
	this->running=false;
}

void HardwareCounterThread::log_counters(){
	std::ofstream f;
	f.open(log_file_name);
	for(unsigned int i=0; i<counter_v.size(); ++i){
		f<<attack_bool_v[i];
		f<<",";
		f<<counter_v[i].c0;
		f<<",";
		f<<counter_v[i].c1;
		f<<",";
		f<<counter_v[i].c2;
		f<<",";
		f<<counter_v[i].c3;
		f<<",";
		f<<counter_v[i].c4;
		f<<",";
		f<<counter_v[i].c5;
		f<<",";
		f<<branch_history_v[i];
		f<<std::endl;
	}
	f.close();
}
		
void HardwareCounterThread::branch_record(int identifier, bool taken){
	// keep a running branch history.
	// it will be recorded when the pmu counters are read.
	// Shift in a 0 or 1 depending on whether the branch is taken or not taken
	branch_history=branch_history<<1;
	branch_history|=(taken ? 1 : 0);
	//std::cout << branch_history << std::endl;

	// keep a map associating branch history with an attack cite or no attack cite
	// the IDENTIFIER is the same as the attack cite for all branches before an attack
	// (after the previous attack)
	// -1 for no attack
	// 0 to 4 for attack
	branch_history_map[identifier]=branch_history;
}
