#include "pmu.h"
#include "Armv8_PMU.h"

using namespace pmu;

// COUNTER API TO HARDWARE THREAD

//TODO make sure these are equivalent to what was done before

void pmu::counter_setup(){
	enable_pmu();
	for(int i=0;i<COUNTER_N; i++) enable_pmn(i);
	// configure counters
	pmn_config(0,0x08); // instructions retired
	pmn_config(1,0x13); // L1 data cache access
	pmn_config(2,0x11); // cpu cycles
	pmn_config(3,0xc9); // conditional branch executed
	pmn_config(4,0xcc); // conditional branch mispredicted
	pmn_config(5,0x1d); // bus cycles
	reset_pmn();
}

void pmu::counter_start(){
	enable_pmu();
}

void pmu::counter_stop(){
	disable_pmu();
}

void pmu::counter_disable(){
	disable_pmu();
}

void pmu::counter_read(counter_t *counter){
	counter->c0=read_pmn(0);
	counter->c1=read_pmn(1);
	counter->c2=read_pmn(2);
	counter->c3=read_pmn(3);
	counter->c4=read_pmn(4);
	counter->c5=read_pmn(5);
}
