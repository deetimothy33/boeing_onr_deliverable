#ifndef PMU_H
#define PMU_H

// ARM pmu counter functions
// 1 read counter
// 2 set up counters
// 3 allow counter modifications

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>

#include <iostream>

#define COUNTER_N 6

namespace pmu{

// holds a variable for each counter
// equates to one reading of counter values
typedef struct{
	unsigned long c0;
	unsigned long c1;
	unsigned long c2;
	unsigned long c3;
	unsigned long c4;
	unsigned long c5;
	unsigned long c6;
	/*double c0;
	double c1;
	double c2;
	double c3;
	double c4;
	double c5;
	double c6;*/

} counter_t;

typedef struct{
	__u32 type;
	__u64 config;
} event_t;

// pick PERF EVENTS
// they will appear in this same order in the LOG FILE
static const event_t event[]={
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS},
	//{PERF_TYPE_HARDWARE, PERF_COUNT_HW_REF_CPU_CYCLES},
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_REFERENCES},
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES},
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_INSTRUCTIONS},
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_MISSES},
	{PERF_TYPE_HARDWARE, PERF_COUNT_HW_BUS_CYCLES},
};

void counter_setup();
void counter_start();
void counter_stop();
void counter_disable();
void counter_read(pmu::counter_t *counter);

}

#endif
