DATA GENERATION COMMANDS
rm $BOD_HOME/ardupilot_data_collection/log/* (OPTIONAL remove previous log files) 
cd $AP_HOME/ArduPlane
sim_vehicles.py
cd $BOD_HOME/ardupilot_data_collection
./generate_data.bash [number of log files to generate]
rm -r interleave_data/in/site*
rm interleave_data/out/aggregate_data.csv
mkdir interleave_data/site0
cp log/* interleave_data/site0
cd interleave_data
./interleave_data.rb
cp out/aggregate_data.csv <whereever it needs to go> 


DATA GENERATION OVERVIEW
Execute sim_vehicles.py in ardupilot/ArduPlane to ensure the code is compiled.
Execute generate_data.bash to create data in ArduPlane/perf_log/loop.csv.
The script moves this log file to ardupilot_data_collection/log/loop_$RANDOM.csv.
Execute interleave_loop.rb to interleave the data from multiple attack sites.
We only have one attack site in the example above.
This script also takes the difference between lines. 
NOTE 	this script prints out the number of lines it didn't use at the end.
	This should be 0 in this case.
The output file is out/aggregate_data.csv containing all the lines from the input data files.
The difference between consecutive lines has been taken.
