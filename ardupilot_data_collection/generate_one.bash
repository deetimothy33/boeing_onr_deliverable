#! /bin/bash

# generates one iteration of the 
# ardupilot data collection process

source ~/.bashrc
	
# start a process to interact with sim_vehicle.py
# this will happen in the background
# sim_vehicle.py will happen in the foreground

(
# WAIT for compilation -- 6 minutes (I timed it)
#sleep 360

# type "mode auto"
# wait
# type "arm throttle"
sleep 100
xdotool type --clearmodifiers $'wp load ../Tools/autotest/CMAC-circuit.txt'
xdotool key Return
sleep 5 
xdotool type --clearmodifiers $'mode auto'
xdotool key Return
sleep 5
xdotool type --clearmodifiers $'arm throttle'
xdotool key Return

# wait for data collection to complete
#sleep 600			
sleep 100

# send SIGTERM to ArduPlane instance
# (and alternative method to do this would be to use "xdotool ctrl+C"
#kill $ARDUPLANE_PID
#xdotool keydown ctrl+c
#xdotool keyup ctrl+c
#xdotool keydown ctrl+c
#xdotool keyup c
#xdotool keyup ctrl
#xdotool key ctrl+c

# kill the process by name
#pkill -15 run_in_terminal
pkill --signal 2 python
#pkill -15 mavproxy.py
#pkill arduplane
) &

# start ArduPlane
# this takes a variable amount of time depending on whether it needs to recompile or not
# this script does not account for recompile time
# make sure the program has been compiled before running this script
#cd ~/ardupilot/ArduPlane
cd $AP_HOME/ArduPlane
#python ~/ardupilot/Tools/autotest/sim_vehicle.py
# get the PID of the last started command
#ARDUPLANE_PID=$!
#/home/pi/ardupilot/Tools/autotest/sim_vehicle.py
$AP_HOME/Tools/autotest/sim_vehicle.py
#sim_vehicle.py 

# log file will be generated after some time in:
# 	~/ardupilot/ArduPlane/logs/*.csv
# move these files to log/*.csv
#mv ~/ardupilot/ArduPlane/perf_log/loop.csv ~/boeing_onr/ardupilot_data_collection/log/loop_$RANDOM.csv
#mv ~/ardupilot/ArduPlane/perf_log/loop.csv ~/boeing_onr/ardupilot_data_collection/log/site0/loop_$RANDOM.csv
#mv ~/ardupilot/ArduPlane/perf_log/task_24.csv ~/boeing_onr/ardupilot_data_collection/log/task_24/task_24_$RANDOM.csv
#mv ~/ardupilot/ArduPlane/perf_log/task_26.csv ~/boeing_onr/ardupilot_data_collection/log/task_26/task_26_$RANDOM.csv
#mv ~/ardupilot/ArduPlane/perf_log/loop.csv ~/boeing_onr/ardupilot_data_collection/log/testing/loop_$RANDOM.csv
#mv $AP_HOME/ArduPlane/perf_log/loop.csv $BOD_HOME/ardupilot_data_collection/log/testing/loop_$RANDOM.csv
mv $AP_HOME/ArduPlane/perf_log/loop.csv $BOD_HOME/ardupilot_data_collection/log/loop_$RANDOM.csv

exit
