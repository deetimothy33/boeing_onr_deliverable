/*---------------------------------------------------------------*/
/* Performance Monitoring Unit (PMU) Example Code for Cortex-A/R */
/*                                                               */
/* Copyright (C) ARM Limited, 2010-2012. All rights reserved.    */
/*---------------------------------------------------------------*/


#ifndef Armv8_PMU
#define Armv8_PMU

#define	ARMV8_PMNC_N_MASK	0x1f /* mask extracts the lower 5(select) bits */
#define	ARMV8_PMNC_MASK		0x3f	 /* Mask for writable bits */

#define	ARMV8_MAX_COUNTERS	32
#define	ARMV8_COUNTER_MASK	(ARMV8_MAX_COUNTERS - 1)

// event   = The event code (from appropriate TRM or ARM Architecture Reference Manual)
// https://developer.arm.com/docs/ddi0500/e/performance-monitor-unit/events

/*---------------------------------------------------------------*/
/*PMCR_EL0 32 BIT REGISTER					 */
/*BIT 0(E)	ENABLE ALL COUNTERS				 */
/*BIT 1(P)	RESET ALL EVENT COUNTERS EXCEPT PMCCNTR_EL0	 */
/*BIT 2(C)	RESET PMCCNTR_EL0				 */
/*BIT 3(D)	CLOCK DIVIDER, WHEN ENABLED PMCCNTR_EL0 COUNTS	 */
/* 		EVERY 64 CLOCK CYCLES				 */
/*---------------------------------------------------------------*/
/*PMCR 	     0 C9 C12 0 CONTROL REG				 */
/*PMCNTENSET 0 C9 C12 1 COUNT ENABLE SET REG			 */
/*PMCNTENCLR 0 C9 C12 2 COUNT ENABLE CLEAR REG			 */
/*PMSELR     0 C9 C12 5 EVENT COUNTER SELECTION REG		 */
/*PMCCNTR    0 C9 C13 0 CYCLE COUNT REG				 */
/*PMXEVTYPER 0 C9 C13 1 SELECTED EVENT TYPE REG			 */
/*PMXEVCNTR  0 C9 C13 2 SELECTED EVENT COUNT REG		 */
/*---------------------------------------------------------------*/

void pmn_config(unsigned long counter, unsigned long eventx)
{
	        counter &=ARMV8_PMNC_N_MASK;
		asm volatile("mcr p15, 0, %0, c9, c12, 5" : : "r"(counter));
		asm volatile("mcr p15, 0, %0, c9, c13, 1" : : "r"(eventx));
}

//EXPORT ccnt_divider
//; Enables / disables the divider(1 / 64) on CCNT
//; void ccnt_divider(int divider)
//; divider(in r0) = If 0 disable divider, else enable divider
void ccnt_divider(int r0)
{
	unsigned long counter;
	unsigned long r1;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter)); //Read PMCR
	if (r0 == 0)
		counter |= 1 << 3;//Set the D bit(enables the divisor)
	else
		counter &= ~(1 << 3);//Clear the D bit(disables the divisor)

	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r"(counter));// Write PMCR
}
// Enables and disables

unsigned long return_PMCR(void)
{
		
	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
	return counter;
}
// Global PMU enable
// On ARM11 this enables the PMU, and the counters start immediately
// On Cortex this enables the PMU, there are individual enables for the counters

void enable_pmu(void)
{
	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
        counter |= 1 << 0;
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (counter));
}
// Global PMU disable
// On Cortex, this overrides the enable state of the individual counters

void disable_pmu(void)

{

	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
	counter &= ~(1 << 0);
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (counter));
}

// Enable the CCNT

void enable_ccnt(void)
{

	unsigned long counter = 0x80000000; /*Set 31st(cycle counter enable) bit of PMCNTENSET REG*/

	asm volatile("mcr   p15, 0, %0, c9, c12, 1" : : "r"(counter));// Write PMCNTENSET Register

}

// Disable the CCNT
void disable_ccnt(void)
{
	unsigned int counter = 0x80000000;
	asm volatile("mcr   p15, 0, %0, c9, c12, 2" : : "r"(counter));// Write PMCNTENSET Register
}

// Enable PMN{n}
// counter = The counter to enable (e.g. 0 for PMN0, 1 for PMN1)
void enable_pmn(unsigned long counter)

{
	//unsigned long x = 1;
	//x = x<<counter;
	counter &= ARMV8_PMNC_N_MASK;
	//counter &= ARMV8_PMNC_N_MASK;
	asm volatile("mcr   p15, 0, %0, c9, c12, 1" : : "r"(1<<counter));// Write PMCNTENSET Register

}
// counter = The counter to enable (e.g. 0 for PMN0, 1 for PMN1)
void disable_pmn(unsigned long counter)
{
	//unsigned long x = 1;
	//x = x<<counter;
	//x &= ARMV8_PMNC_N_MASK;
	counter &= ARMV8_PMNC_N_MASK;
	asm volatile("mcr   p15, 0, %0, c9, c12, 2" : : "r"(1<<counter));// Write PMCNTENSET Register

}
// Read counter values
// Returns the value of CCNT

unsigned long read_ccnt(void)

{
	unsigned long val;
	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r"(val));
	return val;
}

// Returns the value of PMN{n}
// counter = The counter to read (e.g. 0 for PMN0, 1 for PMN1)

unsigned long read_pmn(unsigned long counter)

{
unsigned long val;
counter &= ARMV8_PMNC_N_MASK;
asm volatile("mcr   p15, 0, %0, c9, c12, 5" : : "r"(counter));
asm volatile("mrc p15, 0, %0, c9, c13, 2" : "=r"(val));
return val;
}

void write_pmn(unsigned long counter, unsigned long val)
{
	counter &= ARMV8_PMNC_N_MASK;
	asm volatile("mcr   p15, 0, %0, c9, c12, 5" : : "r"(counter));
	asm volatile("mcr p15, 0, %0, c9, c13, 2" : :"r"(val));
}

// Resets the programmable counters

 void reset_pmn(void)

{
	unsigned long val;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r"(val));
	val |= 1 << 1;
	asm volatile("mcr   p15, 0, %0, c9, c12, 0" : : "r"(val));
}
// Resets the CCNT

void reset_ccnt(void)

{
	unsigned long val;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r"(val));
	val |= 1 << 2;
	asm volatile("mcr   p15, 0, %0, c9, c12, 0" : : "r"(val));

}

unsigned long read_flags()
{
	unsigned long val;
	asm volatile("mrc p15, 0, %0, c9, c12, 3" : "=r"(val));
	return val;

}


#endif // _V7_PMU_H
