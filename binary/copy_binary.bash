#! /bin/bash

# this script copies binaries to their proper places in an ardupilot install
# the ARDUPILOT_HOME variable must be set to the proper location for this to work

#AP_HOME=~/ardupilot
#BOD_HOME=~/boeing_onr_deliverable

#echo $AP_HOME/build/sitl/bin/arduplane
#echo $BOD_HOME/binary/arduplane

cp $BOD_HOME/binary/arduplane $AP_HOME/build/sitl/bin/arduplane
