#!/bin/bash

# Convert original data from .csv to .libsvm
python ./svm_converter/csv2libsvm.py aggregate_data.csv aggregate_data.libsvm 0

# Divide training and testing data
python ./tools/subset.py -s 0 aggregate_data.libsvm 87089 train.data test.data

# Train model
# generate the model file -- train.data.model
./svm-train -s 0 -t 0 train.data

# Copy model file to ArduPilot directory
cp train.data.model $AP_HOME/libraries/AP_Scheduler/

# these commands are not necessary to generate the model file
# they demonstrate the machine learning prediction.

# Training data prediction
./svm-predict train.data train.data.model train.output

# Testing data prediction
./svm-predict test.data train.data.model test.output
  
